package Classes;


import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import ClasseFenetre.MenuResponsableMaintenanceFenetre;

public class ContratDeMaintenance extends JFrame implements ActionListener{
	Container contenu;
	JLabel L1=new JLabel ("Veuillez saisir les informations");
	JLabel L2= new JLabel("Nom du Client");
	JLabel L3= new JLabel ("Dur�e du contrat");
	JLabel L4= new JLabel ("Date du contrat");
	JLabel L5= new JLabel ("Type de maintenance");
	JLabel L6= new JLabel ("Op�ration � effectuer");
	JTextField nomDuClient= new JTextField();
	JTextField DureeDuContrat= new JTextField();
	JTextField DateDuContrat= new JTextField();
	JTextField TypeDeMaintenance= new JTextField();
	JTextField Operation_a_Effectuer= new JTextField();
	JButton bouton=new JButton("Valider");
	public ContratDeMaintenance (){
		this.setBounds(200,200,800,600);
		this.setTitle("Contrat de maintenance");
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		contenu=this.getContentPane();
		contenu.setLayout(null);
		contenu.add(L1);
		contenu.add(L2);
		contenu.add(L3);
		contenu.add(L4);
		contenu.add(L5);
		contenu.add(L6);
		contenu.add(bouton);
		contenu.add(nomDuClient);
		contenu.add(DureeDuContrat);
		contenu.add(DateDuContrat);
		contenu.add(TypeDeMaintenance);
		contenu.add(Operation_a_Effectuer);
		L1.setBounds(250,20,200,50);
		L2.setBounds(40,70,100,50);
		nomDuClient.setBounds(200,80,300,30);
		L3.setBounds(40,130,100,50);
		DureeDuContrat.setBounds(200,140,300,30);
		L4.setBounds(40,190,100,50);
		DateDuContrat.setBounds(200,200,300,30);
		L5.setBounds(40,250,150,50);
		TypeDeMaintenance.setBounds(200,260,300,30);
		L6.setBounds(40,310,175,50);
		Operation_a_Effectuer.setBounds(200,320,300,120);
		bouton.setBounds(380,475,300,50);
		bouton.addActionListener(this);
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	public static void main(String[] args) {
	new ContratDeMaintenance();
	}	
}