package Classes;
import java.sql.Date;
/**
 * @author Lucette
 *
 */
public class FicheMaintenance {
	
	private String titre;
	private String description;
	private Date dateDebut;
	private int type;
	private Devis devis;
	
	public FicheMaintenance() {
		titre = null;
		description = null;
		dateDebut = null;
		type = 0;
		devis = new Devis();
		
	}
	
	/**
	 * 
	 * @param titre
	 * @param description
	 * @param dateDebut
	 * @param type
	 */
	public FicheMaintenance(int type, String titre, String description, Date dateDebut) {
		this.type = type;
		this.titre = titre;
		this.description = description;
		this.dateDebut = dateDebut;
		devis = new Devis();
	}
	
	/**
	 * @return the titre
	 */
	public String getTitre() {
		return titre;
	}
	/**
	 * @param titre the titre to set
	 */
	public void setTitre(String titre) {
		this.titre = titre;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the dateDebut
	 */
	public Date getDateDebut() {
		return dateDebut;
	}
	/**
	 * @param dateDebut the dateDebut to set
	 */
	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}
	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}
	
	

}
