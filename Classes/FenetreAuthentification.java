package Classes;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class FenetreAuthentification extends JFrame implements ActionListener{
	Container contenu;
	JLabel L1=new JLabel ("Veuillez sélectionner un statut");
	JButton bouton = new JButton ("Authentification Responsable de maintenance");
	JButton bouton1 = new JButton ("Authentification Opérateur");
	JButton bouton2 = new JButton ("Valider");
	public FenetreAuthentification (){
		this.setBounds(100,100,600,400);
		this.setTitle("Application de maintenance");
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		contenu=this.getContentPane();
		contenu.setLayout(null);
		contenu.add(L1);
		contenu.add(bouton1);
		contenu.add(bouton);
		contenu.add(bouton2);
		L1.setBounds(205,20,250,50);
		bouton1.setBounds(150,90,300,50);
		bouton.setBounds(150,170,300,50);
		bouton2.setBounds(275, 280, 175, 50);
		bouton.addActionListener(this);
		bouton1.addActionListener(this);
		bouton2.addActionListener(this);
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	public static void main(String[] args) {
		new FenetreAuthentification();
		}
	
}
