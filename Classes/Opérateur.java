package Classes;
/**
 * 
 */

/**
 * @author Lucette FAGNON & Frédéric DUPRE
 *
 */
public class Opérateur {
	
	String nom;
	String prenom;
	String adresse;
	String email;
	String numeroTelephone;
	String numeroDeSecu;
	
	
	/**
	 * @param nom
	 * @param prenom
	 * @param adresse
	 * @param email
	 * @param numeroTelephone
	 * @param numeroDeSecu
	 */
	public Opérateur(String nom, String prenom, String adresse, String email, String numeroTelephone,
			String numeroDeSecu) {
		this.nom = nom;
		this.prenom = prenom;
		this.adresse = adresse;
		this.email = email;
		this.numeroTelephone = numeroTelephone;
		this.numeroDeSecu = numeroDeSecu;
	}
	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return the prenom
	 */
	public String getPrenom() {
		return prenom;
	}
	/**
	 * @param prenom the prenom to set
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	/**
	 * @return the adresse
	 */
	public String getAdresse() {
		return adresse;
	}
	/**
	 * @param adresse the adresse to set
	 */
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the numeroTelephone
	 */
	public String getNumeroTelephone() {
		return numeroTelephone;
	}
	/**
	 * @param numeroTelephone the numeroTelephone to set
	 */
	public void setNumeroTelephone(String numeroTelephone) {
		this.numeroTelephone = numeroTelephone;
	}
	
	/**
	 * @return the numeroDeSecu
	 */
	public String getNumeroDeSecu() {
		return numeroDeSecu;
	}
	/**
	 * @param numeroDeSecu the numeroDeSecu to set
	 */
	public void setNumeroDeSecu(String numeroDeSecu) {
		this.numeroDeSecu = numeroDeSecu;
	}
	

}
