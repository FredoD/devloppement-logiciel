package Classes;
/**
 * 
 */
import java.sql.Date;
/**
 * @author Lucette FAGNON & Fr�d�ric DUPRE
 *
 */
public class Devis {
	
	Date dateDevis;
	double prixHT;
	double prixTTC;
	double tva;
	//String prestations;
	
	/**
	 * Constructor without fields
	 */
	public Devis() {
		this.dateDevis = null;
		this.prixHT = 0;
		this.prixTTC = 0;
		this.tva = 0;
	}
	
	/**
	 * @param dateDevis
	 * @param prixHT
	 * @param prixTTC
	 * @param tva
	 */
	public Devis(Date dateDevis, double prixHT, double prixTTC, double tva) {
		this.dateDevis = dateDevis;
		this.prixHT = prixHT;
		this.prixTTC = prixTTC;
		this.tva = tva;
	}

	/**
	 * @return the dateDevis
	 */
	public Date getDateDevis() {
		return dateDevis;
	}

	/**
	 * @param dateDevis the dateDevis to set
	 */
	public void setDateDevis(Date dateDevis) {
		this.dateDevis = dateDevis;
	}

	/**
	 * @return the prixHT
	 */
	public double getPrixHT() {
		return prixHT;
	}

	/**
	 * @param prixHT the prixHT to set
	 */
	public void setPrixHT(double prixHT) {
		this.prixHT = prixHT;
	}

	/**
	 * @return the prixTTC
	 */
	public double getPrixTTC() {
		return prixTTC;
	}

	/**
	 * @param prixTTC the prixTTC to set
	 */
	public void setPrixTTC(double prixTTC) {
		this.prixTTC = prixTTC;
	}

	/**
	 * @return the tva
	 */
	public double getTva() {
		return tva;
	}

	/**
	 * @param tva the tva to set
	 */
	public void setTva(double tva) {
		this.tva = tva;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Devis [dateDevis=" + dateDevis + ", prixHT=" + prixHT + ", prixTTC=" + prixTTC + ", tva=" + tva + "]";
	}
	
	
	
	
	
	
	

}
