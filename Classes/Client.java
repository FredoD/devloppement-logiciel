package Classes;

public class Client {
	
	private String nomEntreprise;
	private String numeroSiret;
	private String adresse;
	private String codeAPE;
	private String email;
	private String numeroTelephone;
	private FicheMaintenance maintenance;
	private ReportingClient report;
	
		
	/**
	 * Constructor with fields
	 * @param nomEntreprise
	 * 			le nom de l'entreprise client
	 * @param numeroSiret
	 * @param adresse
	 * @param codeAPE
	 * @param email
	 * @param numeroTelephone
	 */
	public Client(String nomEntreprise, String numeroSiret, String adresse, String codeAPE, String email,
			String numeroTelephone) {
		this.nomEntreprise = nomEntreprise;
		this.numeroSiret = numeroSiret;
		this.adresse = adresse;
		this.codeAPE = codeAPE;
		this.email = email;
		this.numeroTelephone = numeroTelephone;
		this.maintenance = new FicheMaintenance();
		this.report= new ReportingClient();
	}
	/**
	 * @return the nomEntreprise
	 */
	public String getNomEntreprise() {
		return nomEntreprise;
	}
	/**
	 * @param nomEntreprise the nomEntreprise to set
	 */
	public void setNomEntreprise(String nomEntreprise) {
		this.nomEntreprise = nomEntreprise;
	}
	/**
	 * @return the numeroSiret
	 */
	public String getNumeroSiret() {
		return numeroSiret;
	}
	/**
	 * @param numeroSiret the numeroSiret to set
	 */
	public void setNumeroSiret(String numeroSiret) {
		this.numeroSiret = numeroSiret;
	}
	/**
	 * @return the adresse
	 */
	public String getAdresse() {
		return adresse;
	}
	/**
	 * @param adresse the adresse to set
	 */
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	/**
	 * @return the codeAPE
	 */
	public String getCodeAPE() {
		return codeAPE;
	}
	/**
	 * @param codeAPE the codeAPE to set
	 */
	public void setCodeAPE(String codeAPE) {
		this.codeAPE = codeAPE;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the numeroTelephone
	 */
	public String getNumeroTelephone() {
		return numeroTelephone;
	}
	/**
	 * @param numeroTelephone the numeroTelephone to set
	 */
	public void setNumeroTelephone(String numeroTelephone) {
		this.numeroTelephone = numeroTelephone;
	}
	/**
	 * Red�finition de la m�thode toString permettant de d�finir la traduction de l'objet en String
	 * pour l'affichage par exemple
	 */
	public String toString() {
		return "Le client [\nNom : " + nomEntreprise + " \nNum�ro de siret: " + numeroSiret
				+ " \nAdresse: " + adresse + " \nCode APE: " + codeAPE + " \nEmail: " + email + " \nContact: " + numeroTelephone + " \n] est enregistr� ";
	}
	
	
}
