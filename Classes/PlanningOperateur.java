/**
 * 
 */
package Classes;

import java.util.Date;

/**
 * @author Lucette FAGNON & Fr�d�ric DUPRE
 *
 */
public class PlanningOperateur {

	/**
	 * l'id de l'op�rateur
	 */
	private int idOperateur;
	
	/**
	 * l'id de la maintenance affect�e � l'op�rateur
	 */
	private int idMaintenance;
	
	/**
	 * la date 
	 */
	private Date date;

	/**
	 * Constructor without fields
	 */
	public PlanningOperateur() {
		this.idOperateur=0;
		this.idMaintenance=0;
		this.date=null;
	}

	/**
	 * @param idOperateur
	 * @param idMaintenance
	 * @param date
	 */
	public PlanningOperateur(int idOperateur, int idMaintenance, Date date) {
		this.idOperateur = idOperateur;
		this.idMaintenance = idMaintenance;
		this.date = date;
	}

	/**
	 * @return the idOperateur
	 */
	public int getIdOperateur() {
		return idOperateur;
	}

	/**
	 * @param idOperateur the idOperateur to set
	 */
	public void setIdOperateur(int idOperateur) {
		this.idOperateur = idOperateur;
	}

	/**
	 * @return the idMaintenance
	 */
	public int getIdMaintenance() {
		return idMaintenance;
	}

	/**
	 * @param idMaintenance the idMaintenance to set
	 */
	public void setIdMaintenance(int idMaintenance) {
		this.idMaintenance = idMaintenance;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "\nPlanningOperateur \nL'id de l'operateur = " + idOperateur + ", \nL'id de la maintenance affect�e = " 
				+ idMaintenance + ", \nDate d'ex�cution de la maintenance = " + date
				+ "\n";
	}
	
	
}
