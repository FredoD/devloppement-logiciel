package Classes;
/**
 * 
 */
import java.sql.Date;

/**
 * @author Lucette FAGNON & Fr�d�ric DUPRE
 *@version 1.0
 */
public class ReportingClient {
	/**
	 * Date of the reporting
	 */
	private Date dateRapport;
	/**
	 * Number of maintenances which were done
	 */
	private int nbreMaintenance;
	/**
	 * Total cost of the maintenances
	 */
	//private double co�tTTC;
	
	public ReportingClient() {
		this.dateRapport = null;
		this.nbreMaintenance = 0;
		//this.co�tTTC = 0;
	}
	
	/**
	 * Constructor of class ReportingClient
	 * @param dateRapport
	 * @param nbreMaintenance
	 * @param co�tTTC
	 */
	public ReportingClient(Date dateRapport, int nbreMaintenance) {
		this.dateRapport = dateRapport;
		this.nbreMaintenance = nbreMaintenance;
		//this.co�tTTC = co�tTTC;
	}
	/**
	 * Getter of dateRapport
	 * @return the date of the Reporting
	 */
	public Date getDateRapport() {
		return dateRapport;
	}
	/**
	 * Setter of dateRapport
	 * @param dateRapport the date of the reporting to set
	 */
	public void setDateRapport(Date dateRapport) {
		this.dateRapport = dateRapport;
	}
	/**
	 * @return the number of the maintenances
	 */
	public int getNbreMaintenance() {
		return nbreMaintenance;
	}
	/**
	 * @param nbreMaintenance the number of the maintenances to set
	 */
	public void setNbreMaintenance(int nbreMaintenance) {
		this.nbreMaintenance = nbreMaintenance;
	}
	
	
	
	

}
