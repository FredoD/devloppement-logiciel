package ClasseFenetre;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;


public class MenuResponsableMaintenanceFenetre extends JFrame implements ActionListener{
	/**
	 * numero de version pour classe serialisable. Permet d'eviter le warning
	 * "The serializable class ClientFenetre does not declare a static final serialVersionUID field of type long"
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * conteneur : il accueille les differents composants graphiques de
	 * ReportingClientFenetre
	 */
	private JPanel containerPanel;
	
	/**
	 * bouton d'ajout d'un client
	 */
	private JButton boutonClient;
	/**
	 * bouton d'ajout de la fiche de maintenance
	 */
	private JButton boutonFiche;

	/**
	 * bouton qui permet d'afficher toutes les fiches
	 */
	private JButton boutonReporting;
	
	/**
	 * bouton qui permet d'afficher toutes les contrats
	 */
	private JButton boutonContrat;

	/**
	 * Zone de d�filement pour la zone de texte
	 */
	JScrollPane zoneDefilement;

	/**
	 * Constructeur D�finit la fen�tre et ses composants - affiche la fen�tre
	 */
	public MenuResponsableMaintenanceFenetre() {

		// on fixe le titre de la fen�tre
		this.setTitle("Menu Responsable de Maintenance");
		// initialisation de la taille de la fen�tre
		this.setSize(600, 100);

		// cr�ation du conteneur
		containerPanel = new JPanel();

		// choix du Layout pour ce conteneur
		// il permet de g�rer la position des �l�ments
		// il autorisera un retaillage de la fen�tre en conservant la
		// pr�sentation
		// BoxLayout permet par exemple de positionner les �lements sur une
		// colonne ( LINE_AXIS )
		containerPanel.setLayout(new BoxLayout(containerPanel,
				BoxLayout.LINE_AXIS));

		// choix de la couleur pour le conteneur
		containerPanel.setBackground(Color.GRAY);
		
		JTabbedPane onglets = new JTabbedPane(SwingConstants.TOP);
		onglets.setFont(new Font ("Brush Script MT", Font.ITALIC, 20));
		
		//JLabel titreOnglet1 = new JLabel("Client");
		//onglet1.add(titreOnglet1);
		
		JTabbedPane onglets2 = new JTabbedPane(SwingConstants.TOP);
		JPanel onglet1 = new JPanel();
		onglet1.setPreferredSize(new Dimension(100, 80));
		onglets.add("Clients", onglets2);
		onglets2.addTab("Nouveau Client", onglet1);
		
		JPanel onglet2 = new JPanel();
		//JLabel titreOnglet2 = new JLabel("Reporting");
		//onglet2.add(titreOnglet2);
		onglets.addTab("Fiches de Maintenance", onglet2);
		JPanel onglet3 = new JPanel();
		//JLabel titreOnglet3 = new JLabel("Reporting");
		//onglet3.add(titreOnglet2);
		onglets.addTab("Reportings", onglet3);
		JPanel onglet4 = new JPanel();
		//JLabel titreOnglet4 = new JLabel("Contrat de maintenance");
		//onglet2.add(titreOnglet4);
		onglets.addTab("Contrat", onglet4);
		onglets.setOpaque(true);
		
		containerPanel.add(onglets);
		

		// create an empty combo box with items of type String
		/*JComboBox<String> comboLanguage = new JComboBox<String>();
		 
		// add items to the combo box
		comboLanguage.addItem("English");
		comboLanguage.addItem("French");
		comboLanguage.addItem("Spanish");
		comboLanguage.addItem("Japanese");
		comboLanguage.addItem("Chinese");
		
		containerPanel.add(comboLanguage);*/
		
		/*boutonClient = new JButton("Client");
		boutonFiche = new JButton("Fiches de Maintenance");
		boutonReporting = new JButton("Reportings");
		boutonContrat = new JButton("Contrats");
		
		containerPanel.setLayout(new FlowLayout( ));

		boutonClient.setFont(new Font ("Times New Roman", Font.PLAIN, 20));
		containerPanel.add(boutonClient);
		boutonFiche.setFont(new Font ("Times New Roman", Font.PLAIN, 20));
		//containerPanel.add(Box.createRigidArea(new Dimension(10, 0)));
		containerPanel.add(boutonFiche);
		boutonReporting.setFont(new Font ("Times New Roman", Font.PLAIN, 20));
		//containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(boutonReporting);
		boutonContrat.setFont(new Font ("Times New Roman", Font.PLAIN, 20));
		// introduire une espace constant entre le champ texte et le composant
		// suivant
		//containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(boutonContrat);


		// ajouter une bordure vide de taille constante autour de l'ensemble des
		// composants
		//containerPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		// ajout des �couteurs sur les boutons pour g�rer les �v�nements
		boutonClient.addActionListener(this);
		boutonFiche.addActionListener(this);
		boutonReporting.addActionListener(this);
		boutonContrat.addActionListener(this);*/

		// permet de quitter l'application si on ferme la fen�tre
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setContentPane(containerPanel);

		// affichage de la fen�tre
		this.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * G�re les actions r�alis�es sur les boutons
	 *
	 */
	/*public void actionPerformed(ActionEvent ae) {
		int retour; // code de retour de la classe ReportingClientDAO

		try {
			if (ae.getSource() == boutonAjouter) {
				// on cr�e l'objet message
				ReportingClient r = new ReportingClient(
						Date.valueOf(this.textFieldDate.getText()),
						maintenanceDAO.getNombreMaintenance(unClientDAO.verifierClient(this.textFieldSiret.getText()))
						);
						
				// on demande � la classe de communication d'envoyer la fiche de maintenance
				// dans la table maintenance_mtn
				retour = reportingDAO.ajouter(r, unClientDAO.verifierClient(this.textFieldSiret.getText()));
				// affichage du nombre de lignes ajout�es
				// dans la bdd pour v�rification
				System.out.println("" + retour + " ligne ajout�e ");
				if (retour == 1)
					JOptionPane.showMessageDialog(this, "reporting ajout�e !");
				else
					JOptionPane.showMessageDialog(this, "erreur ajout reporting",
							"Erreur", JOptionPane.ERROR_MESSAGE);
			} /*else if (ae.getSource() == boutonAffichageTousLesReporting) {
				// on demande � la classe ClientDAO d'ajouter le message
				// dans la base de donn�es
				List<ReportingClient> liste = reportingDAO.getListReporting();
				// on efface l'ancien contenu de la zone de texte
				zoneTextListMaintenance.setText("");
				// on affiche dans la console du client les articles re�us
				for (FicheMaintenance f : liste) {
					zoneTextListMaintenance.append(f.toString());
					zoneTextListMaintenance.append("\n");
					// Pour afficher dans la console :
					// System.out.println(c.toString());
				}
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this,
					"Veuillez contr�ler vos saisies", "Erreur",
					JOptionPane.ERROR_MESSAGE);
			System.err.println("Veuillez contr�ler vos saisies");
		}*/
	
	

	}

