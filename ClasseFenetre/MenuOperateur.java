package ClasseFenetre;

import ClasseDAO.*;
import Classes.*;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class MenuOperateur extends JFrame implements ActionListener{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/*************************************************************************************************/
	/**
	 * Composants des �l�ments de la fen�tre
	 * Mon planning
	 * Container
	 */
	private JPanel p1;
	
	/**
	 * zone de texte pour la champ num�ro de s�cu de l'op�rateur
	 */
	private JTextField textFieldNumSecuPlanning;
	
	/**
	 * label num�ro de s�cu
	 */
	private JLabel labelNumSecuPlanning;
	
	/**
	 * bouton valider pour le num�ro de s�cu
	 */
	private JButton boutonValider;
	
	/**
	 * zone texte pour Afficher le planning
	 */
	private JTextArea zoneAffichagePlanning;
	
	/**
	 * zone de d�filement pour la zone d'affichage du planning
	 */
	private JScrollPane zoneDefilementPlanning;
	
	/*************************************************************************************************/

/************************** * LES ONGLETS ************************************************************/
	/**
	 * conteneur : il accueille les differents composants graphiques de
	 * MenuResponsableMaintenanceFenetre
	 */
	private JPanel containerMainPanel;
	
	JPanel onglet11 = new JPanel();
	JPanel onglet12 = new JPanel();
	
	/**
	 * Constructeur D�finit la fen�tre et ses composants - affiche la fen�tre
	 */
	public MenuOperateur(){

	// on fixe le titre de la fen�tre
			this.setTitle("Menu Client");

			// cr�ation du conteneur
			containerMainPanel = new JPanel();

			// choix du Layout pour ce conteneur
			// il permet de g�rer la position des �l�ments
			// il autorisera un retaillage de la fen�tre en conservant la
			// pr�sentation
			// BoxLayout permet par exemple de positionner les �lements sur une
			// colonne ( LINE_AXIS )
			containerMainPanel.setLayout(new GridLayout());

			// choix de la couleur pour le conteneur
			containerMainPanel.setBackground(Color.GRAY);

			JTabbedPane onglets = new JTabbedPane(SwingConstants.TOP);
			onglets.setFont(new Font("Brush Script MT", Font.ITALIC, 20));

			JTabbedPane onglets2 = new JTabbedPane(SwingConstants.TOP);
			onglet11 = new JPanel();
			onglet11.setLayout(null);
			
			onglets.add("Reporting", onglets2);
			onglets2.addTab("Liste des reportings", onglet11);

			JTabbedPane onglets3 = new JTabbedPane(SwingConstants.TOP);
			onglet12 = new JPanel();
			onglet12.setLayout(null);
			
			onglets.addTab("Planning", onglets3);
			onglets3.addTab("Mon planning", onglet12);
			creerOnglet12();
			
			
			onglets.setOpaque(true);

			containerMainPanel.add(onglets);

			// permet de quitter l'application si on ferme la fen�tre
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

			this.setContentPane(containerMainPanel);
			
			// initialisation de la taille de la fen�tre
			this.setSize(800, 700);
			this.setResizable(false);

			// affichage de la fen�tre
			this.setVisible(true);
	
	}
	
	public void creerOnglet12() {
		//double prixTVA =0, prixRemise= 0, prix=0;
		// cr�ation du conteneur
		onglet12.setSize(700, 700);
		p1 = new JPanel();

		// choix du Layout pour ce conteneur
		// il permet de g�rer la position des �l�ments
		// il autorisera un retaillage de la fen�tre en conservant la
		// pr�sentation
		// BoxLayout permet par exemple de positionner les �lements sur une
		// colonne ( PAGE_AXIS )
		p1.setLayout(new BoxLayout(p1, BoxLayout.PAGE_AXIS));
		p1.setSize(500, 300);
		
		textFieldNumSecuPlanning = new JTextField();
		labelNumSecuPlanning = new JLabel ("Votre num�ro de s�curit� sociale ");
		boutonValider = new JButton ("Valider");
		
		zoneAffichagePlanning= new JTextArea(20, 30);
		zoneDefilementPlanning = new JScrollPane(zoneAffichagePlanning);
		zoneAffichagePlanning.setEditable(false);
		
		p1.add(Box.createRigidArea(new Dimension(0, 10)));
		// ajout des composants sur le container
		p1.add(labelNumSecuPlanning);
		// introduire une espace constant entre le label et le champ texte
		p1.add(Box.createRigidArea(new Dimension(0, 10)));
		p1.add(textFieldNumSecuPlanning);
		// introduire une espace constant entre le champ texte et le composant
		// suivant
		p1.add(Box.createRigidArea(new Dimension(0, 30)));
		
		p1.add(boutonValider);
		p1.add(Box.createRigidArea(new Dimension(0, 5)));
		p1.add(zoneDefilementPlanning);

		// ajouter une bordure vide de taille constante autour de l'ensemble des
		// composants
		p1.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		// ajout des �couteurs sur les boutons pour g�rer les �v�nements
		boutonValider.addActionListener(this);

		onglet12.add(p1);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		PlanningOperateurDAO plan = new PlanningOperateurDAO();
		try{
			if(arg0.getSource()==boutonValider){
				// on demande � la classe PlanningOperateurDAO d'ajouter le message
				// dans la base de donn�es
				List<PlanningOperateur> liste = plan.getListPlanningOperateur(this.textFieldNumSecuPlanning.getText());
				// on efface l'ancien contenu de la zone de texte
				zoneAffichagePlanning.setText("");
				// on affiche dans la console les plannings
				for (PlanningOperateur planning : liste) {
					zoneAffichagePlanning.append(planning.toString());
					zoneAffichagePlanning.append("\n\n");
				}
			}
		}catch(Exception e){
			
		}
	}
}
