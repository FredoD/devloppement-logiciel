package ClasseFenetre;
import ClasseDAO.*;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import ClasseDAO.ClientDAO;
import ClasseDAO.FicheMaintenanceDAO;
import ClasseDAO.ReportingClientDAO;
import Classes.ReportingClient;

import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Box;

import java.util.List;
import java.sql.Date;


/**
 * Classe ReportingClientFenetre
 * D�finit et ouvre une fenetre qui :
 * 
 *    - Permet l'insertion d'une nouveau reporting dans la table reporting_client_rclt via
 * la saisie du num�ro de siret du client, la date du reporting
 * 
 *    - Permet l'affichage de tous reporting dans la console
 * @author Lucette FAGNON & Fr�d�ric DUPRE
 * @version 1.0
 * */


public class ReportingClientFenetre extends JFrame implements ActionListener{
	/**
	 * numero de version pour classe serialisable. Permet d'eviter le warning
	 * "The serializable class ClientFenetre does not declare a static final serialVersionUID field of type long"
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * conteneur : il accueille les differents composants graphiques de
	 * ReportingClientFenetre
	 */
	private JPanel containerPanel;

	/**
	 * zone de texte pour le champ Date
	 */
	private JTextField textFieldDate;

	/**
	 * zone de texte pour le champ Numero de Siret du Client
	 */
	private JTextField textFieldSiret;
	
	/**
	 * label Date
	 */
	private JLabel labelDate;
	
	/**
	 * label Numero de Siret du client
	 */
	private JLabel labelSiret;
	
	/**
	 * bouton d'ajout de la fiche de maintenance
	 */
	private JButton boutonAjouter;

	/**
	 * bouton qui permet d'afficher toutes les fiches
	 */
	private JButton boutonAffichageTousLesReporting;

	/**
	 * Zone de texte pour afficher les fiches de maintenance
	 */
	JTextArea zoneTextListReporting;

	/**
	 * Zone de d�filement pour la zone de texte
	 */
	JScrollPane zoneDefilement;

	/**
	 * instance de ClientDAO permettant les acc�s � la base de donn�es
	 */
	private ClientDAO unClientDAO;

	/**
	 * instance de FicheMaintenanceDAO permettant les acc�s � la base de donn�es
	 */
	private FicheMaintenanceDAO maintenanceDAO;
	
	/**
	 * instance de ReportingClientDAO permettant les acc�s � la base de donn�es
	 */
	private ReportingClientDAO reportingDAO;

	/**
	 * Constructeur D�finit la fen�tre et ses composants - affiche la fen�tre
	 */
	public ReportingClientFenetre() {
		// on instancie la classe ClientDAO
		this.unClientDAO = new ClientDAO();
		// on instancie la classe FiceMaintenanceDAO
		this.maintenanceDAO = new FicheMaintenanceDAO();
		// on instancie la classe ReportingClientDAO
		this.reportingDAO = new ReportingClientDAO();

		// on fixe le titre de la fen�tre
		this.setTitle("R�aliser le reporting Client");
		// initialisation de la taille de la fen�tre
		this.setSize(400, 200);

		// cr�ation du conteneur
		containerPanel = new JPanel();

		// choix du Layout pour ce conteneur
		// il permet de g�rer la position des �l�ments
		// il autorisera un retaillage de la fen�tre en conservant la
		// pr�sentation
		// BoxLayout permet par exemple de positionner les �lements sur une
		// colonne ( PAGE_AXIS )
		containerPanel.setLayout(new BoxLayout(containerPanel,
				BoxLayout.PAGE_AXIS));

		// choix de la couleur pour le conteneur
		containerPanel.setBackground(Color.GRAY);

		// instantiation des composants graphiques
		textFieldSiret = new JTextField();
		textFieldDate = new JTextField();
		
		boutonAjouter = new JButton("ajouter");
		//boutonAffichageTousLesReporting = new JButton(
				//"afficher tous les reporting du client correspondant � cette p�riode");
		labelSiret = new JLabel("Num�ro de Siret de l'entreprise Client: ");
		labelDate = new JLabel("Date de r�alisation du reporting (AAAA-MM-JJ): ");

		//zoneTextListReporting = new JTextArea(10, 20);
		//zoneDefilement = new JScrollPane(zoneTextListReporting);
		//zoneTextListReporting.setEditable(false);

		containerPanel.add(labelSiret);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldSiret);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		
		// ajout des composants sur le container
		containerPanel.add(labelDate);
		// introduire une espace constant entre le label et le champ texte
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldDate);
		// introduire une espace constant entre le champ texte et le composant
		// suivant
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		containerPanel.add(boutonAjouter);

		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		//containerPanel.add(boutonAffichageTousLesReporting);
		//containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		//containerPanel.add(zoneDefilement);

		// ajouter une bordure vide de taille constante autour de l'ensemble des
		// composants
		containerPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		// ajout des �couteurs sur les boutons pour g�rer les �v�nements
		boutonAjouter.addActionListener(this);
		//boutonAffichageTousLesReporting.addActionListener(this);

		// permet de quitter l'application si on ferme la fen�tre
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setContentPane(containerPanel);

		// affichage de la fen�tre
		this.setVisible(true);
	}

	/**
	 * G�re les actions r�alis�es sur les boutons
	 *
	 */
	public void actionPerformed(ActionEvent ae) {
		int retour; // code de retour de la classe ReportingClientDAO

		try {
			if (ae.getSource() == boutonAjouter) {
				// on cr�e l'objet message
				ReportingClient r = new ReportingClient(
						Date.valueOf(this.textFieldDate.getText()),
						maintenanceDAO.getNombreMaintenance(unClientDAO.verifierClient(this.textFieldSiret.getText()))
						);
						
				// on demande � la classe de communication d'envoyer la fiche de maintenance
				// dans la table maintenance_mtn
				retour = reportingDAO.ajouter(r, unClientDAO.verifierClient(this.textFieldSiret.getText()));
				// affichage du nombre de lignes ajout�es
				// dans la bdd pour v�rification
				System.out.println("" + retour + " ligne ajout�e ");
				if (retour == 1)
					JOptionPane.showMessageDialog(this, "reporting ajout�e !");
				else
					JOptionPane.showMessageDialog(this, "erreur ajout reporting",
							"Erreur", JOptionPane.ERROR_MESSAGE);
			} /*else if (ae.getSource() == boutonAffichageTousLesReporting) {
				// on demande � la classe ClientDAO d'ajouter le message
				// dans la base de donn�es
				List<ReportingClient> liste = reportingDAO.getListReporting();
				// on efface l'ancien contenu de la zone de texte
				zoneTextListMaintenance.setText("");
				// on affiche dans la console du client les articles re�us
				for (FicheMaintenance f : liste) {
					zoneTextListMaintenance.append(f.toString());
					zoneTextListMaintenance.append("\n");
					// Pour afficher dans la console :
					// System.out.println(c.toString());
				}
			}*/
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this,
					"Veuillez contr�ler vos saisies", "Erreur",
					JOptionPane.ERROR_MESSAGE);
			System.err.println("Veuillez contr�ler vos saisies");
		}

	}

	public static void main(String[] args) {
		new ReportingClientFenetre();
	}

}
