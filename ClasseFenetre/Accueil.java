package ClasseFenetre;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.Border;

public class Accueil extends JFrame implements ActionListener{
	JButton bouton = new JButton("Valider");
	public Accueil() {
		this.setTitle("Application de maintenance");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.setLayout(new FlowLayout());
		
		JPanel panel = new JPanel(new GridLayout(0, 1));
		Border border = BorderFactory.createTitledBorder("Etes-vous : ");
		panel.setBorder(border);
		
		//ButtonGroup group = new ButtonGroup();
		JRadioButton radio1 = new JRadioButton("Responsable de maintenance");
		radio1.setMnemonic(KeyEvent.VK_1);
		radio1.setActionCommand("Responsable de maintenance");
		radio1.setSelected(true);
		JRadioButton radio2 = new JRadioButton("Opérateur");
		radio2.setMnemonic(KeyEvent.VK_2);
		radio2.setActionCommand("Opérateur");
		
		//group.add(radio1);
		panel.add(radio1);
		//group.add(radio2);
		panel.add(radio2);
		radio1.addActionListener(this);
		radio2.addActionListener(this);
		bouton.setBounds(400,250,50,20);
		
		panel.add(bouton, BorderLayout.EAST);
		
		//panel.setLayout(mgr);;
		Container contentPane = this.getContentPane();
		contentPane.add(panel, BorderLayout.CENTER);
		this.setSize(600, 400);
		
		bouton.addActionListener(this);
		this.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		this.dispose();
		if(arg0.getSource()==bouton){
       Authentification aut= new Authentification();		}
		
	}
}