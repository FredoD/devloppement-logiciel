package ClasseFenetre;


import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import Classes.FenetreAuthentification;

public class InterfaceDevis extends JFrame implements ActionListener{
	Container contenu;
	JLabel L1=new JLabel ("Veuillez saisir les informations");
	JLabel L2= new JLabel("SIRET du Client");
	JLabel L3= new JLabel ("Date du devis");
	JLabel L4= new JLabel ("Prix TTC");
	JLabel L5= new JLabel ("Prix HT");
	JLabel L6= new JLabel ("TVA");
	JLabel L7= new JLabel("Commentaires");
	JTextField SIRET_Du_Client= new JTextField();
	JTextField DateDevis= new JTextField();
	JTextField PrixTTC= new JTextField();
	JTextField PrixHT= new JTextField();
	JTextField TVA= new JTextField();
	JTextField Commentaires = new JTextField();
	JButton bouton=new JButton("Valider");
	public InterfaceDevis (){
		this.setBounds(200,200,800,600);
		this.setTitle("Contrat de maintenance");
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		contenu=this.getContentPane();
		contenu.setLayout(null);
		contenu.add(L1);
		contenu.add(L2);
		contenu.add(L3);
		contenu.add(L4);
		contenu.add(L5);
		contenu.add(L6);
		contenu.add(L7);
		contenu.add(bouton);
		contenu.add(SIRET_Du_Client);
		contenu.add(DateDevis);
		contenu.add(PrixTTC);
		contenu.add(PrixHT);
		contenu.add(TVA);
		contenu.add(Commentaires);
		L1.setBounds(230,20,200,50);
		L2.setBounds(40,70,100,50);
		SIRET_Du_Client.setBounds(175,80,300,30);
		L3.setBounds(40,130,100,50);
		DateDevis.setBounds(175,140,300,30);
		L4.setBounds(40,190,100,50);
		PrixTTC.setBounds(175,200,300,30);
		L5.setBounds(40,250,150,50);
		PrixHT.setBounds(175,260,300,30);
		L6.setBounds(40,310,175,50);
		TVA.setBounds(175,320,300,30);
		L7.setBounds(40,370,175,50);
		Commentaires.setBounds(175,380,300,120);
		bouton.setBounds(400,525,300,50);
		bouton.addActionListener(this);
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	public static void main(String[] args) {
		new InterfaceDevis();
		}
}