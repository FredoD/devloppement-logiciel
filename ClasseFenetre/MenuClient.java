package ClasseFenetre;

import ClasseDAO.*;
import Classes.*;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;


public class MenuClient extends JFrame implements ActionListener{

/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/*************************************************************************************************/
	/**
	 * Composants des �l�ments de la fen�tre
	 * Liste des reportings
	 * Container
	 */
	private JPanel p1;
	
	/**
	 * zone de texte pour la champ num�ro de SIRET du client
	 */
	private JTextField textFieldSiretReporting;
	
	/**
	 * zone de texte pour la date de d�but
	 */
	private JTextField textFieldDateDebut;
	
	/**
	 * zone de texte pour la date de fin
	 */
	private JTextField textFieldDateFin;
	
	/**
	 * label num�ro de SIRET
	 */
	private JLabel labelSiretReporting;
	
	/**
	 * label date de d�but
	 */
	private JLabel labelDateDebut;
	
	/**
	 * label date de fin
	 */
	private JLabel labelDateFin;
	
	/**
	 * label P�riode du Reporting
	 */
	private JLabel labelPeriode;
	
	/**
	 * bouton valider pour le num�ro de SIRET
	 */
	private JButton boutonValider;
	
	/**
	 * zone texte pour Afficher le Reporting
	 */
	private JTextArea zoneAffichageReporting;
	
	/**
	 * zone de d�filement pour la zone d'affichage du planning
	 */
	private JScrollPane zoneDefilementReporting;
	
	/*************************************************************************************************/

/************************** * LES ONGLETS ************************************************************/
	/**
	 * conteneur : il accueille les differents composants graphiques de
	 * MenuResponsableMaintenanceFenetre
	 */
	private JPanel containerMainPanel;
	
	JPanel onglet11 = new JPanel();
	JPanel onglet12 = new JPanel();
	JPanel onglet13 = new JPanel();
	
	/**
	 * Constructeur D�finit la fen�tre et ses composants - affiche la fen�tre
	 */


	public MenuClient(){

	// on fixe le titre de la fen�tre
			this.setTitle("Menu Client");

			// cr�ation du conteneur
			containerMainPanel = new JPanel();

			// choix du Layout pour ce conteneur
			// il permet de g�rer la position des �l�ments
			// il autorisera un retaillage de la fen�tre en conservant la
			// pr�sentation
			// BoxLayout permet par exemple de positionner les �lements sur une
			// colonne ( LINE_AXIS )
			containerMainPanel.setLayout(new GridLayout());

			// choix de la couleur pour le conteneur
			containerMainPanel.setBackground(Color.GRAY);

			JTabbedPane onglets = new JTabbedPane(SwingConstants.TOP);
			onglets.setFont(new Font("Brush Script MT", Font.ITALIC, 20));

			JTabbedPane onglets2 = new JTabbedPane(SwingConstants.TOP);
			onglet11 = new JPanel();
			onglet11.setLayout(null);
			
			onglets.add("Reporting", onglets2);
			onglets2.addTab("Liste des reporting", onglet11);
			creerOnglet11();

			JTabbedPane onglets3 = new JTabbedPane(SwingConstants.TOP);
			onglet12 = new JPanel();
			onglet12.setLayout(null);

			onglets.addTab("Contrat de maintenance", onglets3);
			onglets3.addTab("Contrats", onglet12);
			

			JTabbedPane onglets4 = new JTabbedPane(SwingConstants.TOP);
			onglet13 = new JPanel();
			onglet13.setLayout(null);
		
			onglets.addTab("Devis", onglets4);
			onglets4.addTab("Liste des Devis", onglet13);
			
			onglets.setOpaque(true);

			containerMainPanel.add(onglets);


			// permet de quitter l'application si on ferme la fen�tre
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

			this.setContentPane(containerMainPanel);
			
			// initialisation de la taille de la fen�tre
			this.setSize(800, 700);
			this.setResizable(false);

			// affichage de la fen�tre
			this.setVisible(true);
	
	}
	
	public void creerOnglet11() {
		//double prixTVA =0, prixRemise= 0, prix=0;
		// cr�ation du conteneur
		onglet11.setSize(700, 700);
		p1 = new JPanel();

		// choix du Layout pour ce conteneur
		// il permet de g�rer la position des �l�ments
		// il autorisera un retaillage de la fen�tre en conservant la
		// pr�sentation
		// BoxLayout permet par exemple de positionner les �lements sur une
		// colonne ( PAGE_AXIS )
		p1.setLayout(new BoxLayout(p1, BoxLayout.PAGE_AXIS));
		p1.setSize(500, 500);
		
		textFieldSiretReporting = new JTextField();
		textFieldDateDebut = new JTextField();
		textFieldDateFin = new JTextField();
		labelSiretReporting = new JLabel ("Votre num�ro de SIRET ");
		labelPeriode = new JLabel ("D�finissez la p�riode sur laquelle vous voulez consulter les reporting ");
		labelDateDebut = new JLabel ("Date de d�but ");
		labelDateFin = new JLabel ("Date de Fin ");
		boutonValider = new JButton ("Valider");
		
		zoneAffichageReporting= new JTextArea(20, 30);
		zoneDefilementReporting = new JScrollPane(zoneAffichageReporting);
		zoneAffichageReporting.setEditable(false);
		
		p1.add(Box.createRigidArea(new Dimension(0, 10)));
		// ajout des composants sur le container
		p1.add(labelSiretReporting);
		// introduire une espace constant entre le label et le champ texte
		p1.add(Box.createRigidArea(new Dimension(0, 10)));
		p1.add(textFieldSiretReporting);
		// introduire une espace constant entre le champ texte et le composant
		// suivant
		p1.add(Box.createRigidArea(new Dimension(0, 20)));
		
		p1.add(labelPeriode);
		// introduire une espace constant entre le label et le champ texte
		p1.add(Box.createRigidArea(new Dimension(0, 20)));
		
		p1.add(labelDateDebut);
		// introduire une espace constant entre le label et le champ texte
		p1.add(Box.createRigidArea(new Dimension(0, 10)));
		p1.add(textFieldDateDebut);
		// introduire une espace constant entre le champ texte et le composant
		// suivant
		p1.add(Box.createRigidArea(new Dimension(0, 20)));
		
		p1.add(labelDateFin);
		// introduire une espace constant entre le label et le champ texte
		p1.add(Box.createRigidArea(new Dimension(0, 10)));
		p1.add(textFieldDateFin);
		// introduire une espace constant entre le champ texte et le composant
		// suivant
		p1.add(Box.createRigidArea(new Dimension(0, 30)));
		
		p1.add(boutonValider);
		p1.add(Box.createRigidArea(new Dimension(0, 5)));
		p1.add(zoneDefilementReporting);

		// ajouter une bordure vide de taille constante autour de l'ensemble des
		// composants
		p1.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		// ajout des �couteurs sur les boutons pour g�rer les �v�nements
		boutonValider.addActionListener(this);

		onglet11.add(p1);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
		ReportingClientDAO reporting = new ReportingClientDAO();
		ClientDAO client = new ClientDAO();
		try{
			if(arg0.getSource()==boutonValider){
				// on demande � la classe PlanningOperateurDAO d'ajouter le message
				// dans la base de donn�es
				List<ReportingClient> liste = reporting.getListeReportingClient(Date.valueOf(this.textFieldDateDebut.getText()),
						Date.valueOf(this.textFieldDateFin.getText()), client.verifierClient(this.textFieldSiretReporting.getText()));
				// on efface l'ancien contenu de la zone de texte
				zoneAffichageReporting.setText("");
				// on affiche dans la console les plannings
				for (ReportingClient r : liste) {
					zoneAffichageReporting.append(r.toString());
					zoneAffichageReporting.append("\n\n");
				}
			}
		}catch(Exception e){
			JOptionPane.showMessageDialog(this, "Veuillez contr�ler vos saisies", "Erreur", JOptionPane.ERROR_MESSAGE);
			System.err.println("Veuillez contr�ler vos saisies");
		}
		
	}
	}
