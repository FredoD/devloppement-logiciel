package ClasseFenetre;

import ClasseDAO.*;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import ClasseDAO.ClientDAO;
import ClasseDAO.FicheMaintenanceDAO;
import Classes.FicheMaintenance;

import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Box;

import java.util.List;
import java.sql.Date;

/**
 * Classe FicheMaintenanceFen�tre D�finit et ouvre une fenetre qui :
 * 
 * - Permet l'insertion d'une nouvelle maintenance dans la table maintenance_mtn
 * via la saisie de l'id du client, le titre de la maintenance, sa description,
 * et a date de d�but de la maintenance
 * 
 * - Permet l'affichage de toutes les fiches de maintenance dans la console
 * 
 * @author Lucette FAGNON & Fr�d�ric DUPRE
 * @version 0.1
 */

public class FicheMaintenanceFen�tre extends JFrame implements ActionListener {
	/**
	 * numero de version pour classe serialisable Permet d'eviter le warning
	 * "The serializable class ClientFenetre does not declare a static final
	 * serialVersionUID field of type long"
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * conteneur : il accueille les differents composants graphiques de
	 * ClientFenetre
	 */
	private JPanel containerPanel;

	/**
	 * zone de texte pour le champ Type
	 */
	private JTextField textFieldType;

	/**
	 * zone de texte pour le champ Numero de Siret du Client
	 */
	private JTextField textFieldSiret;

	/**
	 * zone de texte pour le champ Titre
	 */
	private JTextField textFieldTitre;

	/**
	 * zone de texte pour la description de la maintenance
	 * 
	 */
	private JTextField textFieldDescription;
	/**
	 * zone de texte pour la date du d�but de la maintenance
	 */
	private JTextField textFieldDate;

	/**
	 * label Type de la maintenance
	 */
	private JLabel labelType;

	/**
	 * label Numero de Siret du client
	 */
	private JLabel labelSiret;

	/**
	 * label Titre de la maintennce
	 */
	private JLabel labelTitre;

	/**
	 * label Description de la maintenance
	 */
	private JLabel labelDescription;

	/**
	 * label Date de d�but de la maintenance
	 */
	private JLabel labelDate;

	/**
	 * bouton d'ajout de la fiche de maintenance
	 */
	private JButton boutonAjouter;

	/**
	 * bouton qui permet d'afficher toutes les fiches
	 */
	private JButton boutonAffichageToutesLesFiches;

	/**
	 * Zone de texte pour afficher les fiches de maintenance
	 */
	JTextArea zoneTextListMaintenance;

	/**
	 * Zone de d�filement pour la zone de texte
	 */
	JScrollPane zoneDefilement;

	/**
	 * instance de ClientDAO permettant les acc�s � la base de donn�es
	 */
	private ClientDAO unClientDAO;

	/**
	 * instance de FicheMaintenanceDAO permettant les acc�s � la base de donn�es
	 */
	private FicheMaintenanceDAO maintenanceDAO;

	/**
	 * Constructeur D�finit la fen�tre et ses composants - affiche la fen�tre
	 */
	public FicheMaintenanceFen�tre() {
		// on instancie la classe ClientDAO
		this.unClientDAO = new ClientDAO();
		// on instancie la classe FiceMaintenanceDAO
		this.maintenanceDAO = new FicheMaintenanceDAO();

		// on fixe le titre de la fen�tre
		this.setTitle("Fiche de maintenance");
		// initialisation de la taille de la fen�tre
		this.setSize(800, 800);

		// cr�ation du conteneur
		containerPanel = new JPanel();

		// choix du Layout pour ce conteneur
		// il permet de g�rer la position des �l�ments
		// il autorisera un retaillage de la fen�tre en conservant la
		// pr�sentation
		// BoxLayout permet par exemple de positionner les �lements sur une
		// colonne ( PAGE_AXIS )
		containerPanel.setLayout(new BoxLayout(containerPanel, BoxLayout.PAGE_AXIS));

		// choix de la couleur pour le conteneur
		containerPanel.setBackground(Color.ORANGE);

		// instantiation des composants graphiques
		textFieldSiret = new JTextField();
		textFieldType = new JTextField();
		textFieldTitre = new JTextField();
		textFieldDescription = new JTextField();
		textFieldDate = new JTextField();

		boutonAjouter = new JButton("ajouter");
		boutonAffichageToutesLesFiches = new JButton("afficher toutes les fiches de maintenance");
		labelSiret = new JLabel("Num�ro de Siret de l'entreprise Client: ");
		labelType = new JLabel(
				"Type de la maintenance : " + "1- Pr�ventive " + "2- Palliative " + "3- Curative " + " ");
		labelTitre = new JLabel("Titre de la maintenance :");
		labelDescription = new JLabel("Description de la maintenance :");
		labelDate = new JLabel("Date de d�but de la maintenance: ");

		zoneTextListMaintenance = new JTextArea(10, 20);
		zoneDefilement = new JScrollPane(zoneTextListMaintenance);
		zoneTextListMaintenance.setEditable(false);

		containerPanel.add(labelSiret);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldSiret);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		// ajout des composants sur le container
		containerPanel.add(labelType);
		// introduire une espace constant entre le label et le champ texte
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldType);
		// introduire une espace constant entre le champ texte et le composant
		// suivant
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		containerPanel.add(labelTitre);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldTitre);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		containerPanel.add(labelDescription);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldDescription);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		containerPanel.add(labelDate);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldDate);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		containerPanel.add(boutonAjouter);

		containerPanel.add(Box.createRigidArea(new Dimension(0, 20)));

		containerPanel.add(boutonAffichageToutesLesFiches);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(zoneDefilement);

		// ajouter une bordure vide de taille constante autour de l'ensemble des
		// composants
		containerPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		// ajout des �couteurs sur les boutons pour g�rer les �v�nements
		boutonAjouter.addActionListener(this);
		boutonAffichageToutesLesFiches.addActionListener(this);

		// permet de quitter l'application si on ferme la fen�tre
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setContentPane(containerPanel);

		// affichage de la fen�tre
		this.setVisible(true);
	}

	/**
	 * G�re les actions r�alis�es sur les boutons
	 *
	 */
	public void actionPerformed(ActionEvent ae) {
		int retour; // code de retour de la classe ArticleDAO

		try {
			if (ae.getSource() == boutonAjouter) {
				// on cr�e l'objet message
				FicheMaintenance f = new FicheMaintenance(Integer.parseInt(this.textFieldType.getText()),
						this.textFieldTitre.getText(), this.textFieldDescription.getText(),
						Date.valueOf(this.textFieldDate.getText()));

				// on demande � la classe de communication d'envoyer la fiche de
				// maintenance
				// dans la table maintenance_mtn
				retour = maintenanceDAO.ajouter(f, unClientDAO.verifierClient(this.textFieldSiret.getText()));
				// affichage du nombre de lignes ajout�es
				// dans la bdd pour v�rification
				System.out.println("" + retour + " ligne ajout�e ");
				if (retour == 1)
					JOptionPane.showMessageDialog(this, "fiche ajout�e !");
				else
					JOptionPane.showMessageDialog(this, "erreur ajout client", "Erreur", JOptionPane.ERROR_MESSAGE);
			} else if (ae.getSource() == boutonAffichageToutesLesFiches) {
				// on demande � la classe ClientDAO d'ajouter le message
				// dans la base de donn�es
				List<FicheMaintenance> liste = maintenanceDAO.getListeMaintenance();
				// on efface l'ancien contenu de la zone de texte
				zoneTextListMaintenance.setText("");
				// on affiche dans la console du client les articles re�us
				for (FicheMaintenance f : liste) {
					zoneTextListMaintenance.append(f.toString());
					zoneTextListMaintenance.append("\n");
					// Pour afficher dans la console :
					// System.out.println(c.toString());
				}
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, "Veuillez contr�ler vos saisies", "Erreur", JOptionPane.ERROR_MESSAGE);
			System.err.println("Veuillez contr�ler vos saisies");
		}

	}

	public static void main(String[] args) {
		new FicheMaintenanceFen�tre();
	}

}
