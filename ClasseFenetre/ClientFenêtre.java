package ClasseFenetre;
import ClasseDAO.*;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import ClasseDAO.ClientDAO;
import Classes.Client;

import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Box;

import java.util.List;


/**
 * Classe ClientFenetre
 * D�finit et ouvre une fenetre qui :
 * 
 *    - Permet l'insertion d'un nouveau client dans la table client_clt via
 * la saisie du nom, du num�ro de siret, del'adresse du client, de son coe APE, 
 * de son email et de son num�ro de t�l�phone
 *    - Permet l'affichage de tous les clients dans la console
 * @author Lucette FAGNON & Fr�d�ric DUPRE
 * @version 0.1
 * */


public class ClientFen�tre extends JFrame implements ActionListener {
	
	/**
	 * numero de version pour classe serialisable Permet d'eviter le warning
	 * "The serializable class ClientFenetre does not declare a static final serialVersionUID field of type long"
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * conteneur : il accueille les differents composants graphiques de
	 * ClientFenetre
	 */
	private JPanel containerPanel;

	/**
	 * zone de texte pour le champ nom
	 */
	private JTextField textFieldNomEntreprise;

	/**
	 * zone de texte pour le champ num�ro de Siret
	 */
	private JTextField textFieldNumeroSiret;

	/**
	 * zone de texte pour l'adresse de l'entreprise client
	 * 
	 */
	private JTextField textFieldAdresse;
	/**
	 * zone de texte pour le code APE de l'entreprise client
	 */
	private JTextField textFieldCodeAPE;
	
	/**
	 * zone de texte pour l'email du client
	 */
	private JTextField textFieldEmail;
	
	/**
	 * zone de texte pour le num�ro de t�l�phone du client
	 */
	private JTextField textFieldPhone;

	/**
	 * label nom de Entreprise
	 */
	private JLabel labelNomEntreprise;

	/**
	 * label numero de Siret
	 */
	private JLabel labelNumeroSiret;

	/**
	 * label adresse
	 */
	private JLabel labelAdresse;

	/**
	 * label code APE
	 */
	private JLabel labelCodeAPE;
	
	/**
	 * label email
	 */
	private JLabel labelEmail;
	
	/**
	 * label numero de T�l�phone
	 */
	private JLabel labelPhone;

	/**
	 * bouton d'ajout du client
	 */
	private JButton boutonAjouter;

	/**
	 * bouton qui permet d'afficher tous les clients
	 */
	private JButton boutonAffichageTousLesClients;

	/**
	 * Zone de texte pour afficher les clients
	 */
	JTextArea zoneTextListClient;

	/**
	 * Zone de d�filement pour la zone de texte
	 */
	JScrollPane zoneDefilement;

	/**
	 * instance de ClientDAO permettant les acc�s � la base de donn�es
	 */
	private ClientDAO unClientDAO;

	/**
	 * Constructeur D�finit la fen�tre et ses composants - affiche la fen�tre
	 */
	public ClientFen�tre() {
		// on instancie la classe Article DAO
		this.unClientDAO = new ClientDAO();
		      
		// on fixe le titre de la fen�tre
		this.setTitle("Client");
		// initialisation de la taille de la fen�tre
		this.setSize(800, 800);

		// cr�ation du conteneur
		containerPanel = new JPanel();

		// choix du Layout pour ce conteneur
		// il permet de g�rer la position des �l�ments
		// il autorisera un retaillage de la fen�tre en conservant la
		// pr�sentation
		// BoxLayout permet par exemple de positionner les �lements sur une
		// colonne ( PAGE_AXIS )
		containerPanel.setLayout(new BoxLayout(containerPanel,
				BoxLayout.PAGE_AXIS));

		// choix de la couleur pour le conteneur
		//containerPanel.setBackground(Color.PINK);

		// instantiation des composants graphiques
		textFieldNomEntreprise = new JTextField();
		textFieldNumeroSiret = new JTextField();
		textFieldAdresse = new JTextField();
		textFieldCodeAPE = new JTextField();
		textFieldEmail = new JTextField();
		textFieldPhone = new JTextField();
		boutonAjouter = new JButton("ajouter");
		boutonAffichageTousLesClients = new JButton(
				"afficher tous les clients");
		labelNomEntreprise = new JLabel("Nom de l'entreprise :");
		labelNumeroSiret = new JLabel("Num�ro de Siret :");
		labelAdresse = new JLabel("Adresse :");
		labelCodeAPE = new JLabel("Code APE :");
		labelEmail = new JLabel("Email :");
		labelPhone = new JLabel("Contact :");

		zoneTextListClient = new JTextArea(10, 20);
		zoneDefilement = new JScrollPane(zoneTextListClient);
		zoneTextListClient.setEditable(false);

		// ajout des composants sur le container
		containerPanel.add(labelNomEntreprise);
		// introduire une espace constant entre le label et le champ texte
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldNomEntreprise);
		// introduire une espace constant entre le champ texte et le composant
		// suivant
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		containerPanel.add(labelNumeroSiret);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldNumeroSiret);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		containerPanel.add(labelAdresse);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldAdresse);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		containerPanel.add(labelCodeAPE);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldCodeAPE);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		
		containerPanel.add(labelEmail);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldEmail);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		
		containerPanel.add(labelPhone);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldPhone);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		containerPanel.add(boutonAjouter);

		containerPanel.add(Box.createRigidArea(new Dimension(0, 20)));

		containerPanel.add(boutonAffichageTousLesClients);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(zoneDefilement);

		// ajouter une bordure vide de taille constante autour de l'ensemble des
		// composants
		containerPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		// ajout des �couteurs sur les boutons pour g�rer les �v�nements
		boutonAjouter.addActionListener(this);
		boutonAffichageTousLesClients.addActionListener(this);

		// permet de quitter l'application si on ferme la fen�tre
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setContentPane(containerPanel);

		// affichage de la fen�tre
		this.setVisible(true);
	}


	/**
	 * G�re les actions r�alis�es sur les boutons
	 *
	 */
	public void actionPerformed(ActionEvent ae) {
		int retour; // code de retour de la classe ArticleDAO

		try {
			if (ae.getSource() == boutonAjouter) {
				// on cr�e l'objet message
				Client c = new Client(
						this.textFieldNomEntreprise.getText(),
						this.textFieldNumeroSiret.getText(),
						this.textFieldAdresse.getText(),
						this.textFieldCodeAPE.getText(),
						this.textFieldEmail.getText(),
						this.textFieldPhone.getText());
				// on demande � la classe de communication d'envoyer le client
				// dans la table client_clt
				retour = unClientDAO.ajouter(c);
				// affichage du nombre de lignes ajout�es
				// dans la bdd pour v�rification
				System.out.println("" + retour + " ligne ajout�e ");
				if (retour == 1)
					JOptionPane.showMessageDialog(this, "client ajout� !");
				else
					JOptionPane.showMessageDialog(this, "erreur ajout client",
							"Erreur", JOptionPane.ERROR_MESSAGE);
			} else if (ae.getSource() == boutonAffichageTousLesClients) {
				// on demande � la classe ClientDAO d'ajouter le message
				// dans la base de donn�es
				List<Client> liste = unClientDAO.getListeClient();
				// on efface l'ancien contenu de la zone de texte
				zoneTextListClient.setText("");
				// on affiche dans la console du client les articles re�us
				for (Client c : liste) {
					zoneTextListClient.append(c.toString());
					zoneTextListClient.append("\n");
					// Pour afficher dans la console :
					// System.out.println(c.toString());
				}
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this,
					"Veuillez contr�ler vos saisies", "Erreur",
					JOptionPane.ERROR_MESSAGE);
			System.err.println("Veuillez contr�ler vos saisies");
		}

	}

	/*public static void main(String[] args) {
		new ClientFen�tre();
	}*/

}
