package ClasseFenetre;
import ClasseDAO.*;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import ClasseDAO.ClientDAO;
import ClasseDAO.FicheMaintenanceDAO;
import ClasseDAO.ReportingClientDAO;
import Classes.ReportingClient;

import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.BoxLayout;
import javax.swing.Box;

import java.util.List;
import java.sql.Date;


/**
 * Classe ReportingClientFenetre
 * D�finit et ouvre une fenetre qui :
 * 
 *    - Permet l'insertion d'une nouveau reporting dans la table reporting_client_rclt via
 * la saisie du num�ro de siret du client, la date du reporting
 * 
 *    - Permet l'affichage de tous reporting dans la console
 * @author Lucette FAGNON & Fr�d�ric DUPRE
 * @version 1.0
 * */


public class Authentification extends JFrame implements ActionListener{
	/**
	 * numero de version pour classe serialisable. Permet d'eviter le warning
	 * "The serializable class ClientFenetre does not declare a static final serialVersionUID field of type long"
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * conteneur : il accueille les differents composants graphiques de
	 * ReportingClientFenetre
	 */
	private JPanel containerPanel;

	/**
	 * zone de texte pour le champ identifiant
	 */
	private JTextField identifiant;

	/**
	 * zone de texte pour le champ Mot de Passe
	 */
	JPasswordField MotDePasse= new JPasswordField("");
	
	/**
	 * label Identifiant
	 */
	private JLabel labelIdentifiant;
	
	/**
	 * label Mot de Passe
	 */
	private JLabel labelMotDePasse;
	
	/**
	 * bouton de validation
	 */
	private JButton Valider;
	
	/**
	 * instance de AuthentificationDAO permettant les acc�s � la base de donn�es
	 */
	private AuthentificationDAO authentificationDAO = new AuthentificationDAO();


	/**
	 * Constructeur D�finit la fen�tre et ses composants - affiche la fen�tre
	 */
	public Authentification() {

		// on fixe le titre de la fen�tre
		this.setTitle("Authentification");
		// initialisation de la taille de la fen�tre
		this.setSize(600, 400);

		// cr�ation du conteneur
		containerPanel = new JPanel();

		// choix du Layout pour ce conteneur
		// il permet de g�rer la position des �l�ments
		// il autorisera un retaillage de la fen�tre en conservant la
		// pr�sentation
		// BoxLayout permet par exemple de positionner les �lements sur une
		// colonne ( PAGE_AXIS )
		containerPanel.setLayout(null);

		// choix de la couleur pour le conteneur
		containerPanel.setBackground(Color.GRAY);

		// instantiation des composants graphiques
		identifiant = new JTextField();
		identifiant.setPreferredSize(new Dimension(100,20 ));
		
		Valider = new JButton("Valider");
		//boutonAffichageTousLesReporting = new JButton(
				//"afficher tous les reporting du client correspondant � cette p�riode");
		labelIdentifiant = new JLabel("Identifiant");
		labelMotDePasse = new JLabel("Mot de Passe");

		this.getContentPane().add(containerPanel);

		containerPanel.add(labelIdentifiant);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(identifiant);
		labelIdentifiant.setBounds(40,40,100,50);
		identifiant.setBounds(130,40,300,50);
		
		// ajout des composants sur le container
		containerPanel.add(labelMotDePasse);
		// introduire une espace constant entre le label et le champ texte
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(MotDePasse);
		labelMotDePasse.setBounds(40,110,100,50);
		MotDePasse.setBounds(130,110,300,50);

		//getTitle().setBounds(225,0,100,50);
		
		containerPanel.add(Valider);

		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		
		containerPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		
		Valider.setBounds(400,250,100,50);

		// ajout des �couteurs sur les boutons pour g�rer les �v�nements
		Valider.addActionListener(this);
		//boutonAffichageTousLesReporting.addActionListener(this);

		// permet de quitter l'application si on ferme la fen�tre
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setContentPane(containerPanel);

		// affichage de la fen�tre
		this.setVisible(true);
	}

	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		this.dispose();
		System.out.println(authentificationDAO.verifierAuthentification(this.identifiant.getText(), this.MotDePasse.getText()));
		if(arg0.getSource()==Valider){
			if(authentificationDAO.verifierAuthentification(this.identifiant.getText(), this.MotDePasse.getText())==1){
					JOptionPane.showMessageDialog(this, "Authentification r�ussi !");
					if(authentificationDAO.verifierFonction(this.identifiant.getText())==1){
						new MenuResponsableMaintenanceFenetre();
					}
					else if(authentificationDAO.verifierFonction(this.identifiant.getText())==2){
						new MenuOperateur();
					}
					else if(authentificationDAO.verifierFonction(this.identifiant.getText())==3){
						new MenuClient();
					}
				}
				else if(authentificationDAO.verifierAuthentification(this.identifiant.getText(), this.MotDePasse.getText())==-1){
					JOptionPane.showMessageDialog(this, "Mot de Passe erron� !",
							"Erreur", JOptionPane.ERROR_MESSAGE);
					new Authentification();
				}
			}
			else if(authentificationDAO.verifierAuthentification(this.identifiant.getText(), this.MotDePasse.getText())==2){
				JOptionPane.showMessageDialog(this, "Identifiant erron� !",
						"Erreur", JOptionPane.ERROR_MESSAGE);
				new Authentification();
			}
		}
		
	}
	
	
