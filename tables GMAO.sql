
--$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$&&&
-- Suppression de toutes les tables existantes
-- Pour l'utilisateur connect�
--$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
SET SERVEROUTPUT ON
DECLARE
 CURSOR cur_tab_names IS SELECT table_name 
 FROM user_tables;
 sql_stmt VARCHAR2(100);
BEGIN
 FOR v_tab_name IN cur_tab_names LOOP
  sql_stmt := 'DROP TABLE ' || sql_stmt || v_tab_name.table_name || ' CASCADE CONSTRAINTS';
  EXECUTE IMMEDIATE (sql_stmt);
  DBMS_OUTPUT.PUT_LINE(v_tab_name.table_name || ' supprim�e');
  sql_stmt := NULL;
 END LOOP;
END;
/

--$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$&&&$$$$$$$$$$$$$$$$$$$$$
-- Script de cr�ation des tables pour la base Normandie Qualit'AIR
--$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$&&&$$$$$$$$$$$$$$$$$$$$$$
CREATE TABLE client_clt (
  clt_id NUMBER(2),
  clt_nom VARCHAR2(40),
  clt_siret VARCHAR2(15) CONSTRAINT UN_clt_siret UNIQUE,
  clt_adr VARCHAR2 (30),
  clt_ape VARCHAR2 (5),
  clt_email VARCHAR2 (30),
  clt_phone NUMBER(6),
  clt_mdp VARCHAR2 (30) CONSTRAINT UN_clt_mdp UNIQUE,
  CONSTRAINT clt_pk PRIMARY KEY(clt_id)
);
  
  CREATE TABLE type_maintenance_tmtn (
  tmtn_id NUMBER (2),
  tmtn_type VARCHAR2(30) CONSTRAINT UN_tmtn_type UNIQUE,
  CONSTRAINT tmtn_pk PRIMARY KEY(tmtn_id)
  );
  
  CREATE TABLE maintenance_mtn (
  mtn_id NUMBER(2),
  mtn_clt_id NUMBER(2),
  mtn_tmtn_id NUMBER(2),
  mtn_titre VARCHAR2(100),
  mtn_description VARCHAR2(100),
  mtn_date_debut DATE,
  CONSTRAINT mtn_pk PRIMARY KEY(mtn_id),
  CONSTRAINT mtn_clt_id_fk FOREIGN KEY(mtn_clt_id) REFERENCES client_clt(clt_id),
  CONSTRAINT mtn_tmtn_id_fk FOREIGN KEY(mtn_tmtn_id) REFERENCES type_maintenance_tmtn(tmtn_id)
  );
  
  CREATE TABLE devis_dev (
  dev_id NUMBER(2),
  dev_clt_id NUMBER(2),
  dev_mtn_id NUMBER(2),
  dev_date DATE,
  dev_prix_ht NUMBER(20),
  dev_tva NUMBER(20),
  dev_remise NUMBER(20),
  dev_prix_ttc NUMBER(20),
  dev_commentaire VARCHAR2(100),
  CONSTRAINT devis_pk PRIMARY KEY(dev_id),
  CONSTRAINT dev_mtn_id_fk FOREIGN KEY(dev_mtn_id) REFERENCES maintenance_mtn(mtn_id)
  );
  
  CREATE TABLE reporting_client_rclt (
  rclt_id NUMBER (2), 
  rclt_clt_id NUMBER (2),
  rclt_date DATE, 
  rclt_maintenance NUMBER(3),
  CONSTRAINT rclt_pk PRIMARY KEY (rclt_id),
  CONSTRAINT rclt_clt_id_fk FOREIGN KEY(rclt_clt_id) REFERENCES client_clt(clt_id)
  );
  
  CREATE TABLE operateur_opr (
  opr_id NUMBER (2),
  opr_nom VARCHAR2 (30),
  opr_prenom VARCHAR2 (30),
  opr_adr VARCHAR2 (30),
  opr_email VARCHAR2 (30),
  opr_phone VARCHAR2 (30),
  opr_secu VARCHAR2 (30),
  CONSTRAINT opr_pk PRIMARY KEY (opr_id)
  );
  
  CREATE TABLE user_usr (
  usr_id NUMBER (2),
  usr_nom VARCHAR2 (40),
	usr_prenom VARCHAR2 (40),
	usr_mdp VARCHAR2 (100),
	usr_identifiant VARCHAR2 (100),
  usr_fonction VARCHAR2 (30),
  CONSTRAINT usr_pk PRIMARY KEY (usr_id)
  );

  CREATE TABLE planning_operateur_popt (
  popt_id NUMBER (2),
  popt_opr_id NUMBER (2),
  popt_mtn_id NUMBER (2),
  popt_date DATE,
  CONSTRAINT popt_pk PRIMARY KEY (popt_id),
  CONSTRAINT popt_opr_id_fk FOREIGN KEY (popt_opr_id) REFERENCES operateur_opr(opr_id),
  CONSTRAINT popt_mtn_id_fk FOREIGN KEY (popt_mtn_id) REFERENCES maintenance_mtn(mtn_id)
  );
  
  CREATE TABLE reporting_operateur_ropt (
  ropt_id NUMBER (2), 
  ropt_opr_id NUMBER (2),
  ropt_date DATE, 
  ropt_maintenance NUMBER(3),
  CONSTRAINT ropt_pk PRIMARY KEY (ropt_id),
  CONSTRAINT ropt_opr_id_fk FOREIGN KEY(ropt_opr_id) REFERENCES operateur_opr(opr_id)
  );
  
  CREATE TABLE contrat_maintenance_cmtn (
  cmtn_id NUMBER (2), 
  cmtn_clt_id NUMBER (2),
  cmtn_date_debut DATE, 
  cmtn_date_fin DATE,
  cmtn_tmtn_id NUMBER (2),
  cmtn_operation VARCHAR2 (100),
  CONSTRAINT cmtn_pk PRIMARY KEY (cmtn_id),
  CONSTRAINT cmtn_clt_id_fk FOREIGN KEY(cmtn_clt_id) REFERENCES client_clt(clt_id),
  CONSTRAINT cmtn_tmtn_id_fk FOREIGN KEY(cmtn_tmtn_id) REFERENCES type_maintenance_tmtn(tmtn_id)
  );
  
  