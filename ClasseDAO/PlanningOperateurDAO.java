package ClasseDAO;

import Classes.*;
import java.util.ArrayList;
import java.util.List;

import java.sql.*;

/**
 * Classe d'acc�s aux donn�es contenues dans la table planning_operateur_popt
 * 
 * @author Lucette FAGNON & Fr�d�ric DUPRE
 * @version 1.0
 * */
public class PlanningOperateurDAO {

	/**
	 * Param�tres de connexion � la base de donn�es oracle URL, LOGIN et PASS
	 * sont des constantes
	 */
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN = "GMAO";  //exemple BDD1
	final static String PASS = "gmao";   //exemple BDD1

	
	/**
	 * Constructeur de la classe
	 * 
	 */
	public PlanningOperateurDAO() {
		// chargement du pilote de bases de donn�es
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.err.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}

	}
	

	/**
	 * Permet d'ajouter une t�che dans la table planning_operateur_popt Le mode est auto-commit
	 * par d�faut : chaque insertion est valid�e
	 * 
	 * @param idOperateur
	 *            l'id de l'op�rateur concern�
	 * @param idMaintenance
	 *            l'id de la maintenance attribu�e � l'op�rateur
	 * @param date
	 *            la date d'ex�cution de la maintenance
	 * @return retourne le nombre de lignes ajout�es dans la table
	 */
	public int ajouter(int idOperateur, int idMaintenance, Date date) {
		Connection con = null;
		PreparedStatement ps = null;
		int retour = 0;
		
		// connexion �la base de données
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// pr�paration de l'instruction SQL, chaque ? repr�sente une valeur
			// � communiquer dans l'insertion
			// les getters permettent de r�cup�rer les valeurs des attributs
			// souhait�s
			ps = con.prepareStatement("INSERT INTO planning_operateur_popt (popt_opr_id, popt_mtn_id, popt_date) VALUES (?, ?, ?)");
			ps.setInt(1, idOperateur);
			ps.setInt(2, idMaintenance);
			ps.setDate(3, date);

			// Ex�cution de la requ�te
			retour = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		
		return retour;

	}
	
	/**
	 * Permet de r�cup�rer un planning op�rateur � partir de son num�ro de securit� sociale
	 * 
	 * @param numSecu
	 *            le num�ro de s�curit� sociale de l'op�rateur dont on veut r�cup�rer le planning
	 * @return 	une liste de planning de l'op�rateur
	 * 			null si aucun op�rateur n'est d�tenteur de ce num�ro 
	 */
	public List<PlanningOperateur> getListPlanningOperateur(String numSecu) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<PlanningOperateur> retour = new ArrayList<PlanningOperateur>();

		// connexion � la base de donn�es
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT popt_opr_id FROM planning_operateur_popt INNER JOIN operateur_opr WHERE opr_secu = ?");
			ps.setString(1, numSecu);

			// on ex�cute la requ�te
			// rs contient un pointeur situ� juste avant la premi�re ligne
			// retourn�e
			rs = ps.executeQuery();
			// passe � la premi�re (et unique) ligne retourn�e
			while (rs.next())
				retour.add(new PlanningOperateur(rs.getInt("popt_opr_id"), rs.getInt("popt_mtn_id"), rs.getDate("popt_date")));
	
		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du ResultSet, du PreparedStatement et de la Connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}
	
}