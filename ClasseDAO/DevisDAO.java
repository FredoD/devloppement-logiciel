package ClasseDAO;

import java.sql.*;

import Classes.*;


/**
 * Classe d'acc�s aux donn�es contenues dans la table devis_dev
 * 
 * @author Lucette FAGNON & Fr�d�ric DUPRE
 * @version 1.0
 * */
public class DevisDAO {

	/**
	 * Param�tres de connexion � la base de donn�es oracle URL, LOGIN et PASS
	 * sont des constantes
	 */
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN = "GMAO";  //exemple BDD1
	final static String PASS = "gmao";   //exemple BDD1

	
	/**
	 * Constructeur de la classe
	 * 
	 */
	public DevisDAO() {
		// chargement du pilote de bases de donn�es
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.err.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}

	}

	/**
	 * Permet d'ajouter un devis dans la table devis_dev Le mode est auto-commit
	 * par d�faut : chaque insertion est valid�e
	 * 
	 * @param devis
	 *            le devis � ajouter
	 * @param idClient
	 *            l'id du client
	 * @param idMaintenance
	 *            l'id de la maintenance
	 * @return retourne le nombre de lignes ajout�es dans la table
	 */
	public int ajouter(Devis devis, int idClient, int idMaintenance) {
		Connection con = null;
		PreparedStatement ps = null;
		int retour = 0;

		// connexion �la base de donn�es
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// pr�paration de l'instruction SQL, chaque ? repr�sente une valeur
			// � communiquer dans l'insertion
			// les getters permettent de r�cup�rer les valeurs des attributs
			// souhait�s
			ps = con.prepareStatement("INSERT INTO devis_dev (dev_clt_id, dev_mtn_id, dev_date, dev_prix_ht, dev_tva, dev_remise, dev_prix_ttc, dev_commentaire) VALUES (?, ?, ?, ?, ?, ?, ?, ?);");
			ps.setInt(1, idClient);
			ps.setInt(2, idMaintenance);
			ps.setDate(3, devis.getDateDevis());
			ps.setInt(4, devis.getPrixHT());
			ps.setInt(5, devis.getTva());
			ps.setInt(6, devis.getRemise());
			ps.setInt(7, devis.getPrixTTC());
			ps.setString(8, devis.getCommentaire());
            System.out.println("bon");
			// Ex�cution de la requ�te
			retour = ps.executeUpdate();
			System.out.println("bonbon");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}
	
	public static void main(String[] args){
		DevisDAO undevis = new DevisDAO();
		Date date;
		date = Date.valueOf("2014-05-15");
		Devis dev = new Devis (date, 12, 13, 15, 16, "rien");
		int result = undevis.ajouter(dev, 1, 2);
		System.out.println(result);
		
	}
	

}	