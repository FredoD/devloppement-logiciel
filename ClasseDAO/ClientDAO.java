package ClasseDAO;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import Classes.Client;
import java.util.Random;

/**
 * Classe d'acc�s aux donn�es contenues dans la table client_clt
 * 
 * @author Lucette FAGNON & Fr�d�ric DUPRE
 * @version 1.0
 * */
public class ClientDAO {

	/**
	 * Param�tres de connexion � la base de donn�es oracle URL, LOGIN et PASS
	 * sont des constantes
	 */
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN = "GMAO";  //exemple BDD1
	final static String PASS = "gmao";   //exemple BDD1

	
	/**
	 * Constructeur de la classe
	 * 
	 */
	public ClientDAO() {
		// chargement du pilote de bases de donn�es
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.err.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}

	}

	/**
	 * Permet d'ajouter un client dans la table client_clt Le mode est auto-commit
	 * par d�faut : chaque insertion est valid�e
	 * 
	 * @param client
	 *            le client � ajouter
	 * @return retourne le nombre de lignes ajout�es dans la table
	 */
	public int ajouter(Client client) {
		Connection con = null;
		PreparedStatement ps = null;
		int retour = 0;

		// connexion �la base de données
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// pr�paration de l'instruction SQL, chaque ? repr�sente une valeur
			// � communiquer dans l'insertion
			// les getters permettent de r�cup�rer les valeurs des attributs
			// souhait�s
			ps = con.prepareStatement("INSERT INTO client_clt (clt_nom, clt_siret, clt_adr, clt_ape, clt_email, clt_phone, clt_mdp) VALUES (?, ?, ?, ?, ?, ?, ?)");
			ps.setString(1, client.getNomEntreprise());
			ps.setString(2, client.getNumeroSiret());
			ps.setString(3, client.getAdresse());
			ps.setString(4, client.getCodeAPE());
			ps.setString(5, client.getEmail());
			ps.setString(6, client.getNumeroTelephone());
			Random rand = new Random();
			String mdp = client.getNomEntreprise().concat(client.getNumeroSiret());
			int longueur = mdp.length();
			String mdpGen="CLT";
			for(int i = 0; i < 8; i++) {
			  int k = rand.nextInt(longueur);
			  mdpGen = mdpGen+mdp.charAt(k);
			}
			ps.setString(7, mdpGen);

			// Ex�cution de la requ�te
			retour = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	/**
	 * Permet de r�cup�rer un client � partir de son nom
	 * 
	 * @param nomEntreprise
	 *            le nom du client � r�cup�rer
	 * @return 	le client trouv�;
	 * 			null si aucun client ne porte ce nom 
	 */
	public Client getClient(String nomEntreprise) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Client retour = null;

		// connexion � la base de donn�es
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM client_clt WHERE clt_nom = ?");
			ps.setString(1, nomEntreprise);

			// on ex�cute la requ�te
			// rs contient un pointeur situ� juste avant la premi�re ligne
			// retourn�e
			rs = ps.executeQuery();
			// passe � la premi�re (et unique) ligne retourn�e
			if (rs.next())
				retour = new Client(rs.getString("clt_nom"),
						rs.getString("clt_siret"),
						rs.getString("clt_adr"), rs.getString("clt_ape"), 
						rs.getString("clt_email"), rs.getString("clt_phone"));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du ResultSet, du PreparedStatement et de la Connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}
	
	/**
	 * Permet de r�cup�rer l'id d'un client existant dans la table client_clt
	 * @param siret
	 * @return l'id du client s'il existe
	 */
	public int verifierClient(String siret){
		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int retour = 0;

		// connexion � la base de donn�es
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM client_clt WHERE clt_siret = ?");
			ps.setString(1, siret);

			// on ex�cute la requ�te
			// rs contient un pointeur situ� juste avant la premi�re ligne
			// retourn�e
			rs = ps.executeQuery();
			// passe � la premi�re (et unique) ligne retourn�e
			if (rs.next())
				retour = rs.getInt("clt_id");
					
		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du ResultSet, du PreparedStatement et de la Connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;
	}

	/**
	 * Permet de r�cup�rer tous les clients enregistr�s dans la table client_clt
	 * 
	 * @return une ArrayList de clients
	 */
	public List<Client> getListeClient() {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Client> retour = new ArrayList<Client>();

		// connexion � la base de donn�es
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM client_clt");

			// on ex�cute la requ�te
			rs = ps.executeQuery();
			// on parcourt les lignes du r�sultat
			while (rs.next())
				retour.add(new Client(rs.getString("clt_nom"),
						rs.getString("clt_siret"),
						rs.getString("clt_adr"), rs.getString("clt_ape"), 
						rs.getString("clt_email"), rs.getString("clt_phone")));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	// main permettant de tester la classe
	public static void main(String[] args) throws SQLException {

		ClientDAO clientDAO = new ClientDAO();
		// test de la m�thode ajout�
		Client c1 = new Client("RENAULT", "7885588588htt", "12 Rue de la Paix", "14582R", "auxuto@renault.fr",
				"+2552453255");
		int retour = clientDAO.ajouter(c1);

		System.out.println(retour + " lignes ajout�es");

		// test de la m�thode getClient
		Client c2 = clientDAO.getClient("RENAULT");
		System.out.println(c2);

		// test de la m�thode getListeClient
		List<Client> liste = clientDAO.getListeClient();
		// affichage des clients
		for (Client clt : liste) {
			System.out.println(clt.toString());
		}

	}
}
