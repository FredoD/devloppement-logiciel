/**
 * 
 */
package ClasseDAO;
import Classes.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Classe d'acc�s aux donn�es contenues dans la table responsable_maintenance_resp
 * 
 * @author Lucette FAGNON & Fr�d�ric DUPRE
 * @version 1.0
 * */
public class ResponsableMaintenanceDAO {
	
	/**
	 * Param�tres de connexion � la base de donn�es oracle URL, LOGIN et PASS
	 * sont des constantes
	 */
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN = "GMAO";  
	final static String PASS = "gmao";   

	
	/**
	 * Constructeur de la classe
	 * 
	 */
	public ResponsableMaintenanceDAO() {
		// chargement du pilote de bases de donn�es
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.err.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}

	}

	/**
	 * Permet d'ajouter un client dans la table responsable_maintenance_resp Le mode est auto-commit
	 * par d�faut : chaque insertion est valid�e
	 * 
	 * @param responsable
	 *            le responsable � ajouter
	 * @return retourne le nombre de lignes ajout�es dans la table
	 */
	public int ajouter(ResponsableMaintenance responsable) {
		Connection con = null;
		PreparedStatement ps = null;
		int retour = 0;

		// connexion �la base de donn�es
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// pr�paration de l'instruction SQL, chaque ? repr�sente une valeur
			// � communiquer dans l'insertion
			// les getters permettent de r�cup�rer les valeurs des attributs
			// souhait�s
			ps = con.prepareStatement("INSERT INTO responsable_maintenance_resp (resp_nom, resp_prenom, resp_adr, resp_mail, resp_phone, resp_secu) VALUES (?, ?, ?, ?, ?, ?)");
			ps.setString(1, responsable.getNom());
			ps.setString(2, responsable.getPrenom());
			ps.setString(3, responsable.getAdresse());
			ps.setString(4, responsable.getEmail());
			ps.setString(5, responsable.getNumeroTelephone());
			ps.setString(6, responsable.getNumeroDeSecu());

			// Ex�cution de la requ�te
			retour = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}
	
	/**
	 * Permet de r�cup�rer un responsable � partir de son num�ro de S�curit� Sociale
	 * 
	 * @param numSecu
	 *            le num�ro de S�curit� Sociale du responsable � r�cup�rer
	 * @return 	le responsable trouv�;
	 * 			null si aucun responsable ne porte ce nom 
	 */
	public ResponsableMaintenance getResponsable(String numSecu) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ResponsableMaintenance retour = null;

		// connexion � la base de donn�es
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM responsable_maintenance_resp WHERE resp_secu = ?");
			ps.setString(1, numSecu);

			// on ex�cute la requ�te
			// rs contient un pointeur situ� juste avant la premi�re ligne
			// retourn�e
			rs = ps.executeQuery();
			// passe � la premi�re (et unique) ligne retourn�e
			if (rs.next())
				retour = new ResponsableMaintenance(rs.getString("resp_nom"),
						rs.getString("resp_prenom"),
						rs.getString("resp_adr"), rs.getString("resp_mail"), 
						rs.getString("resp_phone"), rs.getString("resp_secu"));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du ResultSet, du PreparedStatement et de la Connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}
	
	/**
	 * Permet de r�cup�rer tous les responsables enregistr�s dans la table client_clt
	 * 
	 * @return une ArrayList de responsable
	 */
	public List<ResponsableMaintenance> getListeResponsable() {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<ResponsableMaintenance> retour = new ArrayList<ResponsableMaintenance>();

		// connexion � la base de donn�es
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM responsable_maintenance_resp");

			// on ex�cute la requ�te
			rs = ps.executeQuery();
			// on parcourt les lignes du r�sultat
			while (rs.next())
				retour.add(new ResponsableMaintenance(rs.getString("resp_nom"),
						rs.getString("resp_prenom"),
						rs.getString("resp_adr"), rs.getString("resp_mail"), 
						rs.getString("resp_phone"), rs.getString("resp_secu")));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

}
