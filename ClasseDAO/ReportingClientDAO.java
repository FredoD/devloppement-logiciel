package ClasseDAO;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import Classes.ReportingClient;

/**
 * Classe d'acc�s aux donn�es contenues dans la table reporting_client_rclt
 * 
 * @author Lucette FAGNON & Fr�d�ric DUPRE
 * @version 1.0
 * */
public class ReportingClientDAO {

	/**
	 * Param�tres de connexion � la base de donn�es oracle URL, LOGIN et PASS
	 * sont des constantes
	 */
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN = "GMAO";  //exemple BDD1
	final static String PASS = "gmao";   //exemple BDD1

	
	/**
	 * Constructeur de la classe
	 * 
	 */
	public ReportingClientDAO() {
		// chargement du pilote de bases de donn�es
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.err.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}

	}

	/**
	 * Permet d'ajouter un reporting dans la table reportng_client_clt Le mode est auto-commit
	 * par d�faut : chaque insertion est valid�e
	 * 
	 * @param reporting
	 *            le reporting � ajouter
	 * @return retourne le nombre de lignes ajout�es dans la table
	 */
	public int ajouter(ReportingClient reporting, int idClient) {
		Connection con = null;
		PreparedStatement ps = null;
		int retour = 0;

		// connexion �la base de données
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// pr�paration de l'instruction SQL, chaque ? repr�sente une valeur
			// � communiquer dans l'insertion
			// les getters permettent de r�cup�rer les valeurs des attributs
			// souhait�s
			ps = con.prepareStatement("INSERT INTO reporting_client_rclt (rclt_clt_id, rclt_date, rclt_maintenance) VALUES (?, ?, ?)");
			ps.setInt(1, idClient);
			ps.setDate(2, reporting.getDateRapport());
			ps.setInt(3, reporting.getNbreMaintenance());
			
			// Ex�cution de la requ�te
			retour = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	/**
	 * Permet de r�cup�rer un reporting client � partir de la p�riode
	 * 
	 * @param dateDebut
	 *            la date de d�but de la p�riode
	 * @param dateFin
	 * 			  la date de fin de la p�riode
	 * @return une ArrayList de clients
	 */
	public List<ReportingClient> getListeReportingClient(Date dateDebut, Date dateFin) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<ReportingClient> retour = new ArrayList<ReportingClient>();

		// connexion � la base de donn�es
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM reporting_client_rclt WHERE rclt_date BETWEEN ? AND ?" );
			ps.setDate(1, dateDebut);
			ps.setDate(2, dateFin);

			// on ex�cute la requ�te
			rs = ps.executeQuery();
			// on parcourt les lignes du r�sultat
			while (rs.next())
				retour.add(new ReportingClient(rs.getDate("rclt_date"),
						rs.getInt("rclt_maintenance")));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

}
