package ClasseDAO;

import java.sql.*;


/**
 * Classe d'acc�s aux donn�es contenues dans la table user_usr
 * 
 * @author Lucette FAGNON & Fr�d�ric DUPRE
 * @version 1.0
 * */

public class AuthentificationDAO {

		/**
		 * Param�tres de connexion � la base de donn�es oracle URL, LOGIN et PASS
		 * sont des constantes
		 */
		final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
		final static String LOGIN = "GMAO";  //exemple BDD1
		final static String PASS = "gmao";   //exemple BDD1

		
		/**
		 * Constructeur de la classe
		 * 
		 */
		public AuthentificationDAO() {
			// chargement du pilote de bases de donn�es
			try {
				Class.forName("oracle.jdbc.OracleDriver");
			} catch (ClassNotFoundException e) {
				System.err.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
			}

		}
		
		/**
		 * Permet de r�cup�rer l'id d'un utilisateur existant dans la table user_usr
		 * @param identifiant
		 * @return 1. si le mot de passe est correct 
		 *        -1. si le mot de passe est incorrect
		 * 		   0. si l'identifiant est incorrect 
		 */
		public int verifierAuthentification(String identifiant, String mdp){
			
			Connection con = null;
			PreparedStatement ps = null;
			ResultSet rs = null;
			int retour = 0;

			// connexion � la base de donn�es
			try {

				con = DriverManager.getConnection(URL, LOGIN, PASS);
				ps = con.prepareStatement("SELECT * FROM user_usr WHERE usr_identifiant = ?");
				ps.setString(1, identifiant);
				// on ex�cute la requ�te
				// rs contient un pointeur situ� juste avant la premi�re ligne
				// retourn�e
				rs = ps.executeQuery();
				// passe � la premi�re (et unique) ligne retourn�e
				if (rs.next()){
					if(mdp.equals(rs.getString("usr_mdp")))
						retour = 1;
					else
						retour = -1;
				}
				else{
					retour = 2;
				}
						
			} catch (Exception ee) {
				ee.printStackTrace();
			} finally {
				// fermeture du ResultSet, du PreparedStatement et de la Connexion
				try {
					if (rs != null)
						rs.close();
				} catch (Exception ignore) {
				}
				try {
					if (ps != null)
						ps.close();
				} catch (Exception ignore) {
				}
				try {
					if (con != null)
						con.close();
				} catch (Exception ignore) {
				}
			}
			return retour;
		}
		
		/**
		 * Permet de r�cup�rer la fonction d'un utilisateur existant dans la table user_usr
		 * @param fonction
		 * @return la fonction de l'utilisateur s'il existe
		 */
		public int verifierFonction(String identifiant){
			
			Connection con = null;
			PreparedStatement ps = null;
			ResultSet rs = null;
			int retour = 0;

			// connexion � la base de donn�es
			try {

				con = DriverManager.getConnection(URL, LOGIN, PASS);
				ps = con.prepareStatement("SELECT * FROM user_usr WHERE usr_identifiant = ?");
				ps.setString(1, identifiant);

				// on ex�cute la requ�te
				// rs contient un pointeur situ� juste avant la premi�re ligne
				// retourn�e
				rs = ps.executeQuery();
				// passe � la premi�re (et unique) ligne retourn�e
				if (rs.next()){
					if(rs.getString("usr_fonction").equals("Responsable de maintenance"))
						retour = 1;
					else if(rs.getString("usr_fonction").equals("Op�rateur"))
						retour = 2;
					else if(rs.getString("usr_fonction").equals("Client"))
						retour = 3;
				}
						
			} catch (Exception ee) {
				ee.printStackTrace();
			} finally {
				// fermeture du ResultSet, du PreparedStatement et de la Connexion
				try {
					if (rs != null)
						rs.close();
				} catch (Exception ignore) {
				}
				try {
					if (ps != null)
						ps.close();
				} catch (Exception ignore) {
				}
				try {
					if (con != null)
						con.close();
				} catch (Exception ignore) {
				}
			}
			return retour;
		}
		
		/*public static void main(String[] agrs){
			AuthentificationDAO dao = new AuthentificationDAO();
			
			System.out.println(dao.verifierAuthentification("061284GA", "Gal1500"));
		}*/
}
