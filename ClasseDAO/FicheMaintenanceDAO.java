package ClasseDAO;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import Classes.FicheMaintenance;

/**
 * Classe d'acc�s aux donn�es contenues dans la table maintenance_mtn
 * 
 * @author Lucette FAGNON & Fr�d�ric DUPRE
 * @version 1.0
 * */
public class FicheMaintenanceDAO {

	/**
	 * Param�tres de connexion � la base de donn�es oracle URL, LOGIN et PASS
	 * sont des constantes
	 */
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN = "GMAO";  //exemple BDD1
	final static String PASS = "gmao";   //exemple BDD1

	
	/**
	 * Constructeur de la classe
	 * 
	 */
	public FicheMaintenanceDAO() {
		// chargement du pilote de bases de donn�es
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.err.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}

	}
	

	/**
	 * Permet d'ajouter une fiche de maintenance dans la table maintenabce_mtn Le mode est auto-commit
	 * par d�faut : chaque insertion est valid�e
	 * 
	 * @param maintenance
	 *            la maintenance � ajouter
	 * @return retourne le nombre de lignes ajout�es dans la table
	 */
	public int ajouter(FicheMaintenance maintenance, int idClient) {
		Connection con = null;
		PreparedStatement ps = null;
		int retour = 0;
		
		// connexion �la base de données
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// pr�paration de l'instruction SQL, chaque ? repr�sente une valeur
			// � communiquer dans l'insertion
			// les getters permettent de r�cup�rer les valeurs des attributs
			// souhait�s
			ps = con.prepareStatement("INSERT INTO maintenance_mtn (mtn_clt_id, mtn_tmtn_id, mtn_titre, mtn_description, mtn_date_debut) VALUES (?, ?, ?, ?, ?)");
			ps.setInt(1, idClient);
			ps.setInt(2, maintenance.getType());
			ps.setString(3, maintenance.getTitre());
			ps.setString(4, maintenance.getDescription());
			ps.setDate(5, maintenance.getDateDebut());

			// Ex�cution de la requ�te
			retour = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		
		return retour;

	}

	/**
	 * Permet de r�cup�rer une fiche de maintenance � partir de son titre
	 * 
	 * @param Titre
	 *            le titre de la maintenance � r�cup�rer
	 * @return 	la maintenance trouv�e trouv�;
	 * 			null si aucune maintenance ne porte ce nom 
	 */
	public FicheMaintenance getFicheMaintenance(String Titre) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		FicheMaintenance retour = null;
		
		
		// connexion � la base de donn�es
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM maintenance_mtn WHERE mtn_titre = ?");
			ps.setString(1, Titre);

			// on ex�cute la requ�te
			// rs contient un pointeur situ� juste avant la premi�re ligne
			// retourn�e
			rs = ps.executeQuery();
			// passe � la premi�re (et unique) ligne retourn�e
			if (rs.next())
				retour = new FicheMaintenance(rs.getInt("mtn_tmtn_id"),
						rs.getString("mtn_titre"),
						rs.getString("mtn_description"),
						rs.getDate("mtn_date_debut"));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du ResultSet, du PreparedStatement et de la Connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	/**
	 * Permet de r�cup�rer toutes les fiches de maintenance enregistr�es dans la table maintenance_mtn
	 * 
	 * @return une ArrayList de fiches de maintenance
	 */
	public List<FicheMaintenance> getListeMaintenance() {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<FicheMaintenance> retour = new ArrayList<FicheMaintenance>();

		// connexion � la base de donn�es
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM maintenance_mtn");

			// on ex�cute la requ�te
			rs = ps.executeQuery();
			// on parcourt les lignes du r�sultat
			while (rs.next())
				retour.add(new FicheMaintenance(rs.getInt("mtn_tmtn_id"), rs.getString("mtn_titre"),
						rs.getString("mtn_description"),
						rs.getDate("mtn_date_debut")));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}
	
	/**
	 * Permet de r�cup�rer toutes les fiches de maintenance enregistr�es dans la table maintenance_mtn
	 * 
	 * @return une ArrayList de fiches de maintenance
	 */
	public int getNombreMaintenance(int idClient) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<FicheMaintenance> list = new ArrayList<FicheMaintenance>();
		//int retour=0;

		// connexion � la base de donn�es
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM maintenance_mtn WHERE mtn_clt_id = ?");
			ps.setInt(1, idClient);

			// on ex�cute la requ�te
			rs = ps.executeQuery();
			// on parcourt les lignes du r�sultat
			while (rs.next()){
				list.add(new FicheMaintenance(rs.getInt("mtn_tmtn_id"), rs.getString("mtn_titre"),
						rs.getString("mtn_description"),
						rs.getDate("mtn_date_debut")));
				
			}

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return list.size();

	}
	
	
/*
	// main permettant de tester la classe
	public static void main(String[] args) throws SQLException {

		
		ClientDAO clientDAO = new ClientDAO();
		FicheMaintenanceDAO maintenanceDAO = new FicheMaintenanceDAO();
		// test de la m�thode ajout�
		Client c1 = new Client("CERVEAU", "788555852cerV", "33 Rue John Privo", "459558c", "cerl@renault.fr",
				"+2552453255");
		//FicheMaintenance f1 = new FicheMaintenance (c1, "Maintenance automobile", "Remplacement des pneus avant et viadnge auto", "15/04/15", "Pr�ventive");
		int retour1 = clientDAO.ajouter(c1);
		//int retour2 = maintenanceDAO.ajouter(f1);

		//System.out.println(retour1 + "\n" + retour2 + " lignes ajout�es");

		// test de la m�thode getClient
		Client c2 = clientDAO.getClient("SURGETT");
		FicheMaintenance f2 = maintenanceDAO.getFicheMaintenance("Maintenance automobile");
		System.out.println(f2 +"\n\n"+ c2);

		// test de la m�thode getListeClient
		List<Client> liste = clientDAO.getListeClient();
		// affichage des clients
		for (Client clt : liste) {
			System.out.println(clt.toString());
		}

	}*/
}
