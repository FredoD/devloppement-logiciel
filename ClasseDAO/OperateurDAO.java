package ClasseDAO;
import Classes.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


import java.util.Random;

/**
 * Classe d'acc�s aux donn�es contenues dans la table operateur_opr
 * 
 * @author Lucette FAGNON & Fr�d�ric DUPRE
 * @version 1.0
 * */
public class OperateurDAO {

	/**
	 * Param�tres de connexion � la base de donn�es oracle URL, LOGIN et PASS
	 * sont des constantes
	 */
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN = "GMAO";  //exemple BDD1
	final static String PASS = "gmao";   //exemple BDD1

	
	/**
	 * Constructeur de la classe
	 * 
	 */
	public OperateurDAO() {
		// chargement du pilote de bases de donn�es
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.err.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}

	}

	/**
	 * Permet d'ajouter un op�rateur dans la table operateur_opr Le mode est auto-commit
	 * par d�faut : chaque insertion est valid�e
	 * 
	 * @param operateur
	 *            l'op�rateur � ajouter
	 * @return retourne le nombre de lignes ajout�es dans la table
	 */
	public int ajouter(Op�rateur operateur) {
		Connection con = null;
		PreparedStatement ps = null;
		int retour = 0;

		// connexion �la base de donn�es
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// pr�paration de l'instruction SQL, chaque ? repr�sente une valeur
			// � communiquer dans l'insertion
			// les getters permettent de r�cup�rer les valeurs des attributs
			// souhait�s
			ps = con.prepareStatement("INSERT INTO operateur_opr (opr_nom, opr_prenom, opr_adr, opr_email, opr_phone, opr_secu) VALUES (?, ?, ?, ?, ?, ?)");
			ps.setString(1, operateur.getNom());
			ps.setString(2, operateur.getPrenom());
			ps.setString(3, operateur.getAdresse());
			ps.setString(4, operateur.getEmail());
			ps.setString(5, operateur.getNumeroTelephone());
			ps.setString(6, operateur.getNumeroDeSecu());

			// Ex�cution de la requ�te
			retour = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}
	
	/**
	 * Permet d'ajouter un op�rateur dans la table user_usr Le mode est auto-commit
	 * par d�faut : chaque insertion est valid�e
	 * 
	 * @param operateur
	 *            l'op�rateur � ajouter
	 * @return retourne le nombre de lignes ajout�es dans la table
	 */
	public int ajouterInfoAuth(Op�rateur operateur) {
		Connection con = null;
		PreparedStatement ps = null;
		int retour = 0;

		// connexion �la base de données
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// pr�paration de l'instruction SQL, chaque ? repr�sente une valeur
			// � communiquer dans l'insertion
			// les getters permettent de r�cup�rer les valeurs des attributs
			// souhait�s
			ps = con.prepareStatement("INSERT INTO user_usr (usr_nom, usr_prenom, usr_mdp, usr_identifiant, usr_fonction) VALUES (?, ?, ?, ?, ?)");
			ps.setString(1, operateur.getNom());
			ps.setString(2, operateur.getPrenom());
			Random rand = new Random();
			String mdp = operateur.getNom().concat(operateur.getNumeroDeSecu());
			int longueur = mdp.length();
			String mdpGen="CLT";
			for(int i = 0; i < 8; i++) {
			  int k = rand.nextInt(longueur);
			  mdpGen = mdpGen+mdp.charAt(k);
			}
			ps.setString(3, mdpGen);
			ps.setString(4, operateur.getNumeroDeSecu());
			ps.setString(5, "Op�rateur");
			
			

			// Ex�cution de la requ�te
			retour = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	/**
	 * Permet de r�cup�rer un op�rateur � partir de son num�ro de securit� sociale
	 * 
	 * @param numSecu
	 *            le num�ro de s�curit� sociale de l'op�rateur � r�cup�rer
	 * @return 	l'op�rateur trouv�;
	 * 			null si aucun op�rateur n'est d�tenteur de ce num�ro 
	 */
	public Op�rateur getOperateur(String numSecu) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Op�rateur retour = null;

		// connexion � la base de donn�es
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM operateur_opr WHERE opr_secu = ?");
			ps.setString(1, numSecu);

			// on ex�cute la requ�te
			// rs contient un pointeur situ� juste avant la premi�re ligne
			// retourn�e
			rs = ps.executeQuery();
			// passe � la premi�re (et unique) ligne retourn�e
			if (rs.next())
				retour = new Op�rateur(rs.getString("opr_nom"),
						rs.getString("opr_prenom"),
						rs.getString("opr_adr"), rs.getString("opr_email"), 
						rs.getString("opr_phone"), rs.getString("opr_secu"));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du ResultSet, du PreparedStatement et de la Connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}
	
	/**
	 * Permet de r�cup�rer l'id d'un op�rateur existant dans la table operateur_opr
	 * @param numSecu le num�ro de s�curit� sociale de l'op�rateur
	 * @return l'id de l'op�rateur s'il existe
	 */
	public int verifierOp�rateur(String numSecu){
		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int retour = 0;

		// connexion � la base de donn�es
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM operateur_opr WHERE opr_secu = ?");
			ps.setString(1, numSecu);

			// on ex�cute la requ�te
			// rs contient un pointeur situ� juste avant la premi�re ligne
			// retourn�e
			rs = ps.executeQuery();
			// passe � la premi�re (et unique) ligne retourn�e
			if (rs.next())
				retour = rs.getInt("opr_id");
					
		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du ResultSet, du PreparedStatement et de la Connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;
	}

	/**
	 * Permet de r�cup�rer tous les op�rateurs enregistr�s dans la table operateur_opr
	 * 
	 * @return une ArrayList d'op�rateurs
	 */
	public List<Op�rateur> getListeOperateur() {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Op�rateur> retour = new ArrayList<Op�rateur>();

		// connexion � la base de donn�es
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM operateur_opr");

			// on ex�cute la requ�te
			rs = ps.executeQuery();
			// on parcourt les lignes du r�sultat
			while (rs.next())
				retour.add(new Op�rateur(rs.getString("opr_nom"),
						rs.getString("opr_prenom"),
						rs.getString("opr_adr"), rs.getString("opr_email"), 
						rs.getString("opr_phone"), rs.getString("opr_secu")));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}
}